/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.util.HashMap;

import net.cellingo.utils.conversion.ValueType;

/**
 * This class encapsulates the NCBI BAST HSP element
 * setters must be used for each of three types of properties:
 * Integer, Double, String, and for these three getters are also provided.
 * A generic getProperty() method is provided for both Integer and Double values.
 * @author Cellingo
 * @version 1.0
 */
public class BlastHsp implements Comparable<BlastHsp> {
	private static int hspCount;
	private int hspNumber;
	private HashMap<HspProperty, Object> hspProperties;
	private boolean isAccepted = true;
	private static BlastProgram blastProgram;   
	/**
	 * bean constructor
	 */
	public BlastHsp(){
		init();
	}

	/**
	 * construct with position parameters
	 * @param queryFrom
	 * @param queryTo
	 * @param hitFrom
	 * @param hitTo
	 */
	public BlastHsp( int queryFrom, int queryTo, int hitFrom, int hitTo ){
		init();
		hspProperties.put(HspProperty.HSP_QUERY_FROM, queryFrom);
		hspProperties.put(HspProperty.HSP_QUERY_TO, queryTo);
		hspProperties.put(HspProperty.HSP_HIT_FROM, hitFrom);
		hspProperties.put(HspProperty.HSP_HIT_TO, hitTo);
	}
	
	/**
	 * construct with position parameters and identities/positives
	 * @param queryFrom
	 * @param queryTo
	 * @param hitFrom
	 * @param hitTo
	 * @param identities
	 * @param positives
	 */
	public BlastHsp( int queryFrom, int queryTo, int hitFrom, int hitTo, int identities, int positives ){
		init();
		hspProperties.put(HspProperty.HSP_QUERY_FROM, queryFrom);
		hspProperties.put(HspProperty.HSP_QUERY_TO, queryTo);
		hspProperties.put(HspProperty.HSP_HIT_FROM, hitFrom);
		hspProperties.put(HspProperty.HSP_HIT_TO, hitTo);
		hspProperties.put(HspProperty.HSP_IDENTITIES, identities);
		hspProperties.put(HspProperty.HSP_POSITIVES, positives );
	}
	
	/**
	 * construct with position parameters and identities/positives and e-value
	 * @param queryFrom
	 * @param queryTo
	 * @param hitFrom
	 * @param hitTo
	 * @param identities
	 * @param positives
	 * @param eValue
	 */
	public BlastHsp( int queryFrom, int queryTo, int hitFrom, int hitTo, int identities, int positives, double eValue ){
		init();
		hspProperties.put(HspProperty.HSP_QUERY_FROM, queryFrom);
		hspProperties.put(HspProperty.HSP_QUERY_TO, queryTo);
		hspProperties.put(HspProperty.HSP_HIT_FROM, hitFrom);
		hspProperties.put(HspProperty.HSP_HIT_TO, hitTo);
		hspProperties.put(HspProperty.HSP_IDENTITIES, identities);
		hspProperties.put(HspProperty.HSP_POSITIVES, positives );
		hspProperties.put(HspProperty.HSP_EVALUE, eValue );
	}
	
	private void init(){
		hspCount++;
		this.hspNumber = hspCount;
		this.hspProperties = new HashMap<HspProperty, Object>();
	}
	
	/**
	 * returns the serial number of this HSP
	 * @return hsp number
	 */
	public int getHspNumber(){
		return hspNumber;
	}
	
	/**
	 * field is set when passed through a filter.
	 * @param isAccepted by the filter
	 */
	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	/**
	 * returns the accepted status of this HSP.
	 * Default value is true.
	 * @return the isAccepted
	 */
	public boolean isAccepted() {
		return isAccepted;
	}

	/**
	 * Check whether a given property exists in this HSP
	 * returns true if it exists.
	 * @param hspProperty
	 * @return existence
	 */
	public boolean containsProperty( HspProperty hspProperty ){
		if( hspProperties.containsKey( hspProperty ) ) return true;
		else if( canCalculateDerived( hspProperty ) ) return true;
		return false;
	}
	
	/**
	 * returns whether a derived property can be calculated
	 * @param hspProperty
	 * @return
	 */
	private boolean canCalculateDerived(HspProperty hspProperty) {
		boolean canCalculate = false;
		try{
			calculateDerivedProperty( hspProperty );
			canCalculate = true;
		}
		catch (NoSuchFieldException e) { }
		catch (IllegalArgumentException e) { }
		return canCalculate;		
	}

	/**
	 * returns the (double) value of an Integer or Double type property
	 * @param hspProperty
	 * @return property value
	 */
	public double getPropertyValue( HspProperty hspProperty ) throws NoSuchFieldException, IllegalArgumentException{
		//System.out.println(" returning property value of : " + hspProperty + " with type " + HspProperty.getValueType(hspProperty) );
		if( HspProperty.getValueType( hspProperty ) == ValueType.INTEGER ){
			if( hspProperties.containsKey(hspProperty) ){
				return ( new Double( (Integer)hspProperties.get(hspProperty) ) );
			}
			else if( HspProperty.isDerivedProperty(hspProperty) ){
				calculateDerivedProperty( hspProperty );
				return ( new Double( (Integer)hspProperties.get(hspProperty) ) );
			}
			else throw new NoSuchFieldException("Requested property (" + hspProperty + ") does not exist");
		}
		else if( HspProperty.getValueType( hspProperty ) == ValueType.DOUBLE ){
			if( hspProperties.containsKey(hspProperty) ){
				return (Double)( hspProperties.get(hspProperty) );
			}
			else if( HspProperty.isDerivedProperty(hspProperty) ){
				calculateDerivedProperty( hspProperty );
				return (Double)( hspProperties.get(hspProperty) );
			}
			else throw new NoSuchFieldException("Requested property (" + hspProperty + ") does not exist");
		}
		else{
			throw new IllegalArgumentException("Requested property (" + hspProperty + ") is not an Integer or Double value");
		}
	}
	
	/**
	 * calculates a derived hsp property value
	 * @param hspProperty
	 */
	private void calculateDerivedProperty(HspProperty hspProperty) throws NoSuchFieldException, IllegalArgumentException {
		double val;
		switch( hspProperty ){
			case HSP_ALIGN_PERCENTAGE:
				double queryLength = 
					( BlastHsp.blastProgram == BlastProgram.BLASTX 
							? getPropertyValue( HspProperty.QUERY_LENGTH )/3 
							: getPropertyValue( HspProperty.QUERY_LENGTH ) );
				//System.out.println("BP=" + blastProgram + " al:" + getPropertyValue( HspProperty.HSP_ALIGN_LENGTH ) + " ql: " + queryLength );
				val = ( getPropertyValue( HspProperty.HSP_ALIGN_LENGTH ) / queryLength ) * 100; 
				//save for future reference
				setDoublePropertyValue( HspProperty.HSP_ALIGN_PERCENTAGE, val );
				break;
			case HSP_IDENTITY_PERCENTAGE:
				val = ( getPropertyValue( HspProperty.HSP_IDENTITIES ) / getPropertyValue( HspProperty.HSP_ALIGN_LENGTH ) ) * 100; 
				setDoublePropertyValue(HspProperty.HSP_IDENTITY_PERCENTAGE, val);
				break;
			case HSP_MISMATCH_NUMBER:
				val = ( getPropertyValue( HspProperty.HSP_ALIGN_LENGTH ) - getPropertyValue( HspProperty.HSP_IDENTITIES ) ); 
				setIntegerPropertyValue(HspProperty.HSP_MISMATCH_NUMBER, (int)Math.round(val) );
				break;
			case HSP_POSITIVE_PERCENTAGE:
				val = ( getPropertyValue( HspProperty.HSP_POSITIVES ) / getPropertyValue( HspProperty.HSP_ALIGN_LENGTH ) ) * 100; 
				setDoublePropertyValue(HspProperty.HSP_POSITIVE_PERCENTAGE, val);
				break;
			case HSP_MISMATCH_PERCENTAGE:
				val = ( ( getPropertyValue( HspProperty.HSP_ALIGN_LENGTH ) - getPropertyValue( HspProperty.HSP_IDENTITIES ) ) / getPropertyValue( HspProperty.HSP_ALIGN_LENGTH ) ) * 100; 
				setDoublePropertyValue(HspProperty.HSP_MISMATCH_PERCENTAGE, val);
				break;
			case HSP_GAP_OPENINGS:
				val = getGapOpenings( getStringPropertyValue(HspProperty.HSP_QUERY_SEQUENCE), getStringPropertyValue(HspProperty.HSP_HIT_SEQUENCE) );
				setIntegerPropertyValue(HspProperty.HSP_GAP_OPENINGS, (int)val);
				break;
			default:
				throw new NoSuchFieldException("requested derived property is unavailable: " + hspProperty.name() );
		}
	}
	
	/**
	 * returns the gap openings in the alignment of two seqs seq
	 * @param seq
	 * @return gapOpenings
	 */
	private int getGapOpenings( String querySeq, String hitSeq ){
		/*
		 * <Hsp_qseq>DFFNADSMFMQTMLLPTDAMFTDCESPLYKNKS---GGKNIVTDVGESVLSSSSDEKMSFKVLSHVLRRFPVLLHCNYKQTNT----PLWKELYKHGKFALLGDLVLFSNPFHPNIPAMPFDKSPICDTTGKSIIMSEVMTKELLYKLADKDIGQFFAVLNVTNPITGDSFLHYFAG-GNTMRDGEGDKICTSADVLRIIAEITIQKTGKM</Hsp_qseq>
		 * <Hsp_hseq>NFLEAHKLVVELLLPSYSSDVVYCDSETYTKPIPIFGNKSIVSTIGDYVLSNPNED-VSYQMVSSVLEKFPLLFHCTYKTNEEDKGIPLWKKLYNKRKFKLLNSLLVHNNKNWTPVPAIPFDRENICDASGRSVLMSEIMSTSTFQTICKNNTHYLFDMLNMERGKQGGSFLHFFASRKNSFTNFENEEM--DSHVLSNIAKFICNEKEKL</Hsp_hseq>
		*/
		int gapOpenings = getGapOpenings(querySeq);
		gapOpenings += getGapOpenings(hitSeq);
		return gapOpenings;
	}
	
	/**
	 * returns the gap openings in a single seq
	 * @param seq
	 * @return gapOpenings
	 */
	private int getGapOpenings( String seq ){
		int gapOpenings = 0;
		for( char c : seq.toCharArray() ){
			char prev = '?';
			if( c == '-' && prev != '-'){
				gapOpenings++;
				prev = c;
			}
		}
		return gapOpenings;
	}

	/**
	 * get the value of an Integer type property
	 * @param hspProperty
	 * @return the property value
	 * @throws IllegalArgumentException
	 * @throws NoSuchFieldException
	 */
	public int getIntegerPropertyValue( HspProperty hspProperty ) throws IllegalArgumentException, NoSuchFieldException{
		if( HspProperty.getValueType( hspProperty ) == ValueType.INTEGER ){
			if( hspProperties.containsKey(hspProperty) ){
				return (Integer)( hspProperties.get(hspProperty) );
			}
			else if( HspProperty.isDerivedProperty(hspProperty) ){
				calculateDerivedProperty( hspProperty );
				return ( (Integer)hspProperties.get(hspProperty) );
			}
			else if( isCalculableProperty(hspProperty) ){
				/*try to determine the property from the data that is available*/
				return calculateCalculableIntegerProperty(hspProperty);
			}
			else{
				//System.err.println( this.toString() + "\n\tunable to return property\n\t" + hspProperty );
				throw new NoSuchFieldException("Requested property " +  hspProperty.name() + " (" + hspProperty + ") does not exist for \n\t" + this.toString() + "\n");
			}
		}
		else{
			throw new IllegalArgumentException("Requested property (" + hspProperty + ") is not an Integer value");
		}
	}
	
	private int calculateCalculableIntegerProperty(HspProperty hspProperty) {
		if( hspProperty == HspProperty.HSP_IDENTITIES ){
			int al = (Integer)( hspProperties.get(HspProperty.HSP_ALIGN_LENGTH) );
			int mm = (Integer)(hspProperties.get(HspProperty.HSP_MISMATCH_NUMBER) );
			//System.out.println("HSP identities=" + ( al - mm ));
			return ( al - mm );
		} 
		/*throw exception for incalculable property*/
		throw new IllegalArgumentException( "This property should not be requested: " + hspProperty + " on this BlastHsp:\n\t" + this.toString() );
	}

	private boolean isCalculableProperty( HspProperty hspProperty ){
		//System.out.println(hspProperty + " is calculable?");
		if( hspProperty == HspProperty.HSP_IDENTITIES 
			&& this.containsProperty(HspProperty.HSP_ALIGN_LENGTH) 
			&& this.containsProperty(HspProperty.HSP_MISMATCH_NUMBER) ){
				//System.out.println("yes!!");
				return true;
		}
		//System.out.println("no!!");
		return false;
	}
	
	/**
	 * set a property with an Integer value
	 * @param hspProperty
	 * @param value
	 * @throws IllegalArgumentException
	 */
	public void setIntegerPropertyValue( HspProperty hspProperty, int value ) throws IllegalArgumentException{
		if( HspProperty.getValueType( hspProperty ) == ValueType.INTEGER ){
			hspProperties.put(hspProperty, (Object)value);
		}
		else{
			throw new IllegalArgumentException("Property to set (" + hspProperty + ") is not an Integer value: " + value);
		}
	}

	/**
	 * get the value of an Double type property
	 * 
	 * @param hspProperty
	 * @return property value
	 * @throws IllegalArgumentException
	 * @throws NoSuchFieldException
	 */
	public double getDoublePropertyValue( HspProperty hspProperty ) throws IllegalArgumentException, NoSuchFieldException{
		if( HspProperty.getValueType( hspProperty ) == ValueType.DOUBLE ){
			if( hspProperties.containsKey(hspProperty) ){
				return (Double)( hspProperties.get(hspProperty) );
			}
			else if( HspProperty.isDerivedProperty(hspProperty) ){
				calculateDerivedProperty( hspProperty );
				return (Double)( hspProperties.get(hspProperty) );
			}
			else throw new NoSuchFieldException("Requested property (" + hspProperty + ") does not exist");
		}
		else{
			throw new IllegalArgumentException("Requested property (" + hspProperty + ") is not an Double value");
		}
	}

	/**
	 * set a property with an Double value
	 * @param hspProperty
	 * @param value
	 * @throws IllegalArgumentException
	 */
	public void setDoublePropertyValue( HspProperty hspProperty, double value ) throws IllegalArgumentException{
		if( HspProperty.getValueType( hspProperty ) == ValueType.DOUBLE ){
			hspProperties.put(hspProperty, (Object)value);
		}
		else{
			throw new IllegalArgumentException("Property to set (" + hspProperty + ") is not an Double value: " + value );
		}
	}

	/**
	 * get the value of an String type property
	 * @param hspProperty
	 * @return property value
	 * @throws IllegalArgumentException
	 * @throws NoSuchFieldException
	 */
	public String getStringPropertyValue( HspProperty hspProperty ) throws IllegalArgumentException, NoSuchFieldException{
		if( HspProperty.getValueType( hspProperty ) == ValueType.STRING ){
			if( hspProperties.containsKey(hspProperty) ){
				return (String)( hspProperties.get(hspProperty) );
			}
			else throw new NoSuchFieldException("Requested property (" + hspProperty + ") does not exist");
		}
		else{
			throw new IllegalArgumentException("Requested property (" + hspProperty + ") is not an String value");
		}
	}

	/**
	 * set a property with an String value
	 * @param hspProperty
	 * @param value
	 * @throws IllegalArgumentException
	 */
	public void setStringPropertyValue( HspProperty hspProperty, String value ) throws IllegalArgumentException{
		if( HspProperty.getValueType( hspProperty ) == ValueType.STRING ){
			hspProperties.put(hspProperty, (Object)value);
		}
		else{
			throw new IllegalArgumentException("Property to set (" + hspProperty + ") is not an String value: " + value );
		}
	}

	/**
	 * compares on the basis of e-value 
	 */
	//@Override
	public int compareTo(BlastHsp otherHsp) {
		final int BEFORE = -1;
		final int AFTER = 1;
		final int EQUAL = 0;

		try{
			if( this.getDoublePropertyValue( HspProperty.HSP_EVALUE ) < otherHsp.getDoublePropertyValue( HspProperty.HSP_EVALUE ) ){
				return BEFORE;
			}
			else if( this.getDoublePropertyValue( HspProperty.HSP_EVALUE ) > otherHsp.getDoublePropertyValue( HspProperty.HSP_EVALUE ) ){
				return AFTER;
			}
			else{
				return EQUAL;
			}
			
		} catch(Exception e){
			return EQUAL;
		}
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append( "[ " );
		sb.append( getClass().getSimpleName() );
		sb.append( " ] hsp number=" );
		sb.append( getHspNumber() );
		sb.append( " properties: " );
		sb.append( this.hspProperties.clone().toString() );
		
		return sb.toString();
	}

	/**
	 * sets the blast program used; this is relevant for filtering blastx results
	 * @param blastProgram
	 */
	public static void setBlastProgram(BlastProgram blastProgram) {
		//System.out.println("blast program set:" + blastProgram);
		BlastHsp.blastProgram = blastProgram;
	}

	/**
	 * returns the blast program used; this is relevant for filtering blastx results
	 * @return blastProgram
	 */
	public static BlastProgram getBlastProgram(){
		return BlastHsp.blastProgram;
	}
	
}
