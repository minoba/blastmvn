/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.cellingo.utils.conversion.ValueType;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * The main class for running blast analyses. Must be run with a configuration.xml 
 * command line argument
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 1.0
 */
public class BlastRunner implements ParserListener {
    /**
     * XPaths to the configuration file settings
     */
    public static final String LOGGER_NAME = "log.name";
    public static final String LOG_LEVEL = "log.level";
    public static final String LOG_FILE = "log.file";
    public static final String LOG_CONFIGFILE = "log.properties_file";

    public static final String BLAST_EXECUTABLE = "blast.executable";
    public static final String BLAST_PROGRAM = "blast.program";
    public static final String BLAST_DB = "blast.database";
    public static final String BLAST_ARGS = "blast.blast_arguments";
    public static final String BLAST_MAX_HSPS = "blast.max_hsps";
    
    public static final String INPUT_TYPE = "blast.input_type";
    public static final String INPUT_FILE = "blast.input_file";
    public static final String KEEP_TEMP = "blast.keep_blast_output";
    public static final String BLAST_OUTPUT_FILE = "blast.blast_output_file";
    
    public static final String REPORT_FORMAT = "output.report_format";
    public static final String GENERATE_CSV_FILE_HEADER = "output.generate_csv_file_header";
    public static final String REPORT_FILE = "output.report_file";
    public static final String GENERATE_NO_HIT_LIST = "output.generate_no_hit_list";
    public static final String NO_HIT_FILE = "output.no_hit_file";
    
    public static final String DB_HOST = "database.host";
    public static final String DB_NAME = "database.database_name";
    public static final String DB_USER = "database.user";
    public static final String DB_PASSW = "database.password";
    public static final String DB_QUERY = "database.sequence_query";

	/**
	 * the log4j logger object
	 */
	private static Logger logger;

    /**
     * the name of the config file
     */
	private String configFile;
    /**
     * The configuration settings for the database connection.
     */
    private XMLConfiguration configuration;
    /**
     * the BlastConfiguration object is created form the settings specified 
     * in the configuration xml file
     */
    private BlastConfiguration blastConfiguration;
    /**
     * object to hold all analysis results
     */
    private BlastResults blastResults;
    /**
     * flag to indicate whether the output file has been created
     */
    private boolean hitOutfileCreated = false;
    /**
     * flag to indicate whether the no-hits output file has been created
     */
    private boolean noHitOutfileCreated = false;
    
    private int skippedHsps = 0;
    /**
     * flag indicating whether the application is started from the command line 
     */
    //private boolean clMode = false;
       
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if( args.length != 1){
			System.out.println("no configuration file provided!");
			System.out.println("usage: java -jar BlastRunner.jar <configuration file>");
			System.out.println("aborting");
			System.exit( 1 );
		}
		BlastRunner blastRunner = new BlastRunner( args[0] );
		blastRunner.start();
	}

	/**
	 * construct with the name of the config file
	 * @param configFile
	 */
	public BlastRunner( String configFile ){
		this.configFile = configFile;
		this.blastResults = new BlastResults();
		this.blastConfiguration = new BlastConfiguration();
	}

	/**
	 * construct with pathnames and only other settings in the the config file
	 * @param blastExecutable
	 * @param blastDatabase
	 * @param querySequences
	 * @param blastExecutableOutput
	 * @param blastrunnerHitsFile
	 * @param blastRunnerNohitsFile
	 * @param blastRunnerLogFile
	 * @param configFile
	 */
	public void startCLmode(
			String blastExecutable,
			String blastDatabase,
			String querySequences,
			String blastExecutableOutput,
			String blastrunnerHitsFile,
			String blastRunnerNohitsFile,
			String blastRunnerLogFile){
		//this.clMode = true;

		try {
			loadConfiguration();

			/*attach to configuration object for equal downstream processing*/
			this.configuration.setProperty(BLAST_OUTPUT_FILE, blastExecutableOutput);
			this.configuration.setProperty(INPUT_FILE, querySequences);
			this.configuration.setProperty(REPORT_FILE, blastrunnerHitsFile);
			this.configuration.setProperty(NO_HIT_FILE, blastRunnerNohitsFile);
			this.configuration.setProperty(BLAST_EXECUTABLE, blastExecutable);
			this.configuration.setProperty(BLAST_DB, blastDatabase);
			this.configuration.setProperty(LOG_FILE, blastRunnerLogFile);

			logAnalysisStart();
			
			
			processSettings();
			runBlast();
			//System.out.println("FINISHED");
		} catch (Exception e) {
			e.printStackTrace();
			/*all program exceptions that occur during execution converge here*/
			logger.fatal("An error occurred. Aborting.\nThe following information is available:\n" + e.getMessage() );
			System.out.println("An error occurred. Aborting. See log for details.");
			cleanUp();
			System.exit( 1 );
		}
		
	}
	/**
	 * start the analysis procedure
	 */
	private void start(){
		//this.clMode = false;
		try {
			loadConfiguration();
			logAnalysisStart();
			this.configuration.setProperty(BLAST_EXECUTABLE, "blastall");
			
			processSettings();
			runBlast();
			
		} catch (Exception e) {
			e.printStackTrace();
			/*all program exceptions that occur during execution converge here*/
			logger.fatal("An error occurred. Aborting.\nThe following information is available:\n" + e.getMessage() );
			System.out.println("An error occurred. Aborting. See log for details.");
			cleanUp();
			System.exit( 1 );
		}
	}
	
	private void runBlast() throws Exception{
		if( blastConfiguration.isGenerateNoHitsList() ){
			createQueries();
		}
		
		doBlast();
		
		logger.info("Starting Blast output parsing");
		BlastParser parser = new NcbiBlastParser( );
		parser.setMaxHsps(blastConfiguration.getMaxHspNumber());
		parser.setFile( blastConfiguration.getTempOutputFile() );
		parser.setBlastResults( blastResults );
		parser.parseStreaming( this );
		parser.parse();
		logger.info("Blast output parsing finished");

		if( blastConfiguration.isGenerateNoHitsList() ){
			writeQueriesWithoutBlastHits();
		}
		if( !hitOutfileCreated ){
			File outputFile = blastConfiguration.getFilteredOutputFile();
			if( ! (outputFile.createNewFile() || outputFile.canWrite() ) ){
				throw new IOException("[BlastRunner.processFilteredQuery()] can not create or write to report output file");
			}
			hitOutfileCreated = true;
			
			if( blastConfiguration.getOutputFormat().equalsIgnoreCase(BlastConfiguration.FILETYPE_CSV) 
					&& blastConfiguration.isGenerateHeaderLine() ){
				writeFileHeader();
			}
		}
		cleanUp();

		logger.info("Analysis finished. " + skippedHsps + " HSPs were skipped because of missing data fields. Exiting normally");
	}
	
	/**
	 * closes and cleans up some resources 
	 */
	private void cleanUp(){
		if( ! blastConfiguration.isKeepBlastOutput() ){
			File tempBlastOut = blastConfiguration.getTempOutputFile();
			tempBlastOut.delete();
		}
	}
	
	/**
	 * if a no-hit list is requested, write the remaining queries to the no-hit list
	 * @throws Exception
	 */
	private void writeQueriesWithoutBlastHits() throws Exception{
		logger.info("writing results");
		BufferedWriter nhOut = null;
		try {
			
			File noHitsFile = blastConfiguration.getNoHitsFile();
			
			if( ! noHitOutfileCreated ){
				if( ! (noHitsFile.createNewFile() || noHitsFile.canWrite() ) ){
					throw new Exception("[BlastRunner.writeQueriesWithoutBlastHits()] can not write to no-hits output file");
				}
				noHitOutfileCreated = true;
			}
			
			nhOut = new BufferedWriter( new FileWriter(noHitsFile, true) );
			Iterator<BlastQuery> queries = blastResults.getQueries();
			while( queries.hasNext() ){
				BlastQuery query = queries.next();
				//System.out.println("query: " + query.getQueryId() + " has hit: " + query.hasHits() );
				if( query.hasHits() ){
					logger.error("data inconsistency: there should be no more queries with hits!");
					throw new Exception("data inconsistency: there should be no more queries with hits!");
				}
				else{
					writeQueryWithoutHits( nhOut, query );
				}
			}
		} catch (Exception e1) {
			throw new Exception( "[BlastRunner.writeQueriesWithoutBlastHits()] exception during processing of filtered blast results: " + e1.getMessage() + "; cause of exception: " + e1.getCause() );
		}
		finally{
			nhOut.close();
		}
	}
	
	//@Override
	public void iterationReady( BlastQuery query ) {
		/*this method implements the streaming blast results processing*/ 
		try {
			logger.debug( "processing query " + query.getQueryId());
			//System.out.println( "processing blast results of query " + query.getQueryId() );
			filterQuery( query );
			processFilteredQuery( query );
			//clean up to recycle heap space
			blastResults.removeQuery( query );
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * process a filtered query
	 * @param query
	 * @throws Exception 
	 */
	private void processFilteredQuery( BlastQuery query ) throws IOException{
		logger.debug("writing results of query " + query.getQueryId() );
		
		String outputFormat = blastConfiguration.getOutputFormat();
		List<HspProperty> outputFields = blastConfiguration.getOutputFields();

		//System.out.println("output format: " + outputFormat );
		if( outputFormat.equalsIgnoreCase(BlastConfiguration.FILETYPE_CSV) ){
			BufferedWriter out = null;
			BufferedWriter nhOut = null;
			try {
				File outputFile = blastConfiguration.getFilteredOutputFile();
				/*create the output file if not yet created*/
				if( !hitOutfileCreated ){
					if( ! (outputFile.createNewFile() || outputFile.canWrite() ) ){
						throw new IOException("[BlastRunner.processFilteredQuery()] can not create or write to report output file");
					}
					hitOutfileCreated = true;
					
					if( blastConfiguration.getOutputFormat().equalsIgnoreCase(BlastConfiguration.FILETYPE_CSV) 
							&& blastConfiguration.isGenerateHeaderLine() ){
						writeFileHeader();
					}
				}
				
				out = new BufferedWriter( new FileWriter(outputFile, true) );
				
				if(blastConfiguration.isGenerateNoHitsList() && !query.hasHits() ){
					
					File noHitsFile = blastConfiguration.getNoHitsFile();
					
					if( ! noHitOutfileCreated ){
						if( ! (noHitsFile.createNewFile() || noHitsFile.canWrite() ) ){
							throw new Exception("[BlastRunner.writeQueriesWithoutBlastHits()] can not write to no-hits output file");
						}
						noHitOutfileCreated = true;
					}
					
					nhOut = new BufferedWriter( new FileWriter(noHitsFile, true) );
					writeQueryWithoutHits( nhOut, query );
					nhOut.close();
				}
				if( query.hasHits() ){
					writeQueryWithHits( out, query, outputFields );
				}
			} catch (Exception e1) {
				e1.printStackTrace();
				throw new IOException( "[BlastRunner.processFilteredResults()] exception during processing of filtered blast results: " + e1.getMessage() + "; cause of exception: " + e1.getCause() );
			}
			finally{
				out.close();
			}
		}
	}
	
	/**
	 * filter a query
	 * @param query
	 * @throws NoSuchFieldException
	 */
	private void filterQuery( BlastQuery query ) throws NoSuchFieldException {
		logger.debug("filtering blast results of query " + query.getQueryId() );

		HspPropertiesFilter filter = blastConfiguration.getHspFilter();
		try{
			Iterator<BlastHit> hits = query.getBlastHits();
			//int hitCount = 0;
			while(hits.hasNext()){
				BlastHit hit = hits.next();
				
				logger.debug("filtering query " + query.getQueryId() + " with hit " + hit.getHitDefinition() );
				
				//hitCount++;
				Iterator<BlastHsp> hsps = hit.getHsps();
				while(hsps.hasNext()){
					BlastHsp hsp = hsps.next();
					//now filter HSPs and write to output if OK
					hsp.setAccepted( filter.filter( hsp ) );
					//System.out.println("hit:" + hit.getHitDefinition() );
					//System.out.println("hit first HSP OK: " + blastConfiguration.getHspFilter().filter( hit.getFirstHsp() ) );
					if( ! hsp.isAccepted() ){
						hit.removeHsp( hsp );
					}
				}
				if( hit.isEmpty() ){
					query.removeHit( hit );
				}
			}
		}
		catch (NoSuchFieldException e) {
			throw new NoSuchFieldException( "[BlastRunner.filterQuery()] exception during filtering of blast results: " + e.getMessage() + "; cause of exception: " + e.getCause() );
			//e.printStackTrace();
		}
	}

	/**
	 * write a header line to the output csv file
	 * @throws Exception
	 */
	private void writeFileHeader() throws Exception{
		if( blastConfiguration.isGenerateHeaderLine() ){
			File outputFile = blastConfiguration.getFilteredOutputFile();
			BufferedWriter out = null;
			try{
				out = new BufferedWriter( new FileWriter(outputFile) );
				if( BlastHsp.getBlastProgram() == BlastProgram.BLASTX ) out.write("#SOURCE="+BlastHsp.getBlastProgram()+"\n");
				out.write("QUERY_ID" ); 
				
				for( HspProperty property : blastConfiguration.getOutputFields() ){
					out.write( "\t" + property.name() );
				}
				out.write( "\n" );
			}
			catch (Exception e) {
				throw new Exception( "[BlastRunner.writeFileHeader()] exception during writing of header line to output file: " + e.getCause() );
			}
			finally{
				out.close();
			}
		}
	}
	
	/**
	 * Write a query without hits to the specified output stream.
	 * @param out
	 * @param query
	 * @throws IOException
	 */
	private void writeQueryWithoutHits( BufferedWriter out, BlastQuery query ) throws IOException{
		out.write("" + query.getQueryId() + "\n"); 
	}
	
	/**
	 * Write a query with hits to the specified output stream and in the selected format.
	 * The fields that are written to out are obtained from the BlastConfiguration object, 
	 * which in turn has been constructed from the configuration.xml file. If no fields have been set in 
	 * the configuration file, a default field set is used. 
	 * @param out
	 * @param query
	 * @param outputFields 
	 * @throws Exception 
	 */
	private void writeQueryWithHits( BufferedWriter out, BlastQuery query, List<HspProperty> outputFields ) throws Exception{
		//System.out.println("writing results of query: " + query.getQueryId() );
		Iterator<BlastHit> hits = query.getBlastHits();
		//int hitCount = 0;
		while(hits.hasNext()){
			/*iterate over hits*/
			BlastHit hit = hits.next();
			//System.out.println( "   " + this.getClass().getSimpleName() + " hit:" + hit.getHitDefinition());
			//hitCount++;
			Iterator<BlastHsp> hsps = hit.getHsps();
			while(hsps.hasNext()){
				/*iterate over HSPs*/
				BlastHsp hsp = hsps.next();
				//System.out.println( "     " + this.getClass().getSimpleName() + " hsp: " + hsp.getHspNumber() );
				//now filter HSPs and write to output if OK
				if( hsp.isAccepted() ){
					StringBuilder sb = new StringBuilder();
					try{
						//out.write("" + query.getQueryId() );
						sb.append( "" + query.getQueryId() );
						
						for( HspProperty property : outputFields ){
							//System.out.println("\t" + property);
							
							if( property == HspProperty.HIT_ID ){
								//out.write("\t" + hit.getHitID() );
								sb.append( "\t" + hit.getHitID() );
							}
							else if( property == HspProperty.HIT_DEFINITION ){
								//out.write("\t" + hit.getHitDefinition() );
								sb.append( "\t" + hit.getHitDefinition() );
							}
							else if( property == HspProperty.HIT_ACCESSION ){
								//out.write("\t" + hit.getHitAccession() );
								sb.append( "\t" + hit.getHitAccession() );
							}
							else if( property == HspProperty.HIT_LENGTH ){
								//out.write("\t" + hit.getHitLength() );
								sb.append( "\t" + hit.getHitLength() );
							}
							else if( HspProperty.getValueType( property ) == ValueType.INTEGER ){
								//System.out.println( property + " has value " + hsp.getIntegerPropertyValue( property ));
								//out.write( "\t" + hsp.getIntegerPropertyValue( property ) );
								sb.append( "\t" + hsp.getIntegerPropertyValue( property ) );
							}
							else if( HspProperty.getValueType( property ) == ValueType.DOUBLE ){
								//NumberFormat nf = NumberFormat.getNumberInstance( new Locale("en","US") );
								//nf.setMaximumFractionDigits(10);
								//DecimalFormat formatter = new DecimalFormat("##.########");
								//out.write( "\t" + nf.format( hsp.getDoublePropertyValue( property ) ) );
								//out.write( "\t" + hsp.getDoublePropertyValue( property ) );
								sb.append( "\t" + hsp.getDoublePropertyValue( property ) );
							}
							else if( HspProperty.getValueType( property ) == ValueType.STRING ){
								//out.write( "\t" + hsp.getStringPropertyValue( property ) );
								sb.append( "\t" + hsp.getStringPropertyValue( property ) );
							}
						}
						//out.write( "\n" );
						sb.append( "\n" );
						
						/*only when this point is reached should the HSP be written to out*/
						out.write( sb.toString() );
						
					}catch (NoSuchFieldException e) {
						/*if a HSP does not have the required field, skip it*/
						e.printStackTrace();
						logger.error(" HSP " + hsp.getHspNumber() + " has a missing or incorrect datafield and will be skipped:\n" + hsp.toString());
						skippedHsps++;
						//e.printStackTrace();
						//throw new Exception( "[BlastRunner.writeQueryWithHits()] exception during writing of blast hit results: " + e.getMessage() 
						//		+ "; cause of exception: " + e.getCause() );
					}
				}
			}
			//break;
		}
		//System.exit(0);
	}
	
	/**
	 * perfom the actual Blast analysis
	 */
	private void doBlast() throws Exception{
		logger.info("Starting BLAST process");
		/**/
		String s = null;
        Process ps;
		try {
			ps = Runtime.getRuntime().exec( blastConfiguration.getBlastCommand() );
	        BufferedReader psStdInput = new BufferedReader( new InputStreamReader(ps.getInputStream()) );
	        BufferedReader psStdError = new BufferedReader( new InputStreamReader(ps.getErrorStream()) );

	        /* read the output from the command */
	        while ((s = psStdInput.readLine()) != null) {
	        	logger.debug("STDOUT from BLAST: " + s);
	        }
	        /* read any errors from the attempted command */
	        if( (s = psStdError.readLine()) != null){
	        	StringBuilder sberr = new StringBuilder();
	        	sberr.append( "STDERR from BLAST: " + s + "\n" );
	        	if( s.contains("FATAL ERROR")){
			        logger.error( sberr.toString() );
	        		throw new Exception("[BlastRunner.doBlast()] : an error occurred running BLAST");
	        	}
		        while ((s = psStdError.readLine()) != null) {
		        	sberr.append( s + "\n" );
		        }
		        logger.error( sberr.toString() );
	        }
		} catch (IOException e) {
			//e.printStackTrace();
			throw new Exception("[BlastRunner.doBlast()] BLAST run failed");
		}
		ps.getInputStream().close();
		ps.getErrorStream().close();
		logger.info("BLAST process finished");
	}

	/**
	 * creates a list of queries with status of "NO_HIT"
	 * @throws Exception 
	 */
	private void createQueries() throws Exception{
		File queryFile = blastConfiguration.getInputSequenceFile();
		try {
			BufferedReader br =  new BufferedReader(new FileReader(queryFile));
			String line;
			while( (line = br.readLine()) != null ){
				if(line.startsWith(">")){
					blastResults.createQuery(line.substring(1));//QueryId( line.substring(1) );
					//System.out.println("query created: " + blastResults.getQuery( line.substring(1) ).getQueryId() );
				}
			}
			br.close();
		} catch (FileNotFoundException e) {
			throw new Exception( "[BlastRunner.createQueries()] sequence input file is not existing: " + queryFile.getName() + "; cause of exception:" + e.getCause() );
		} catch (IOException e) {
			throw new Exception( "[BlastRunner.createQueries()] reading of sequence input file is not possible: " + queryFile.getName()  + "; cause of exception:" + e.getCause());
		}
	}
	
	/**
	 * processes the settings and generates a BlastFilter object
	 */
//	private void processCLSettings() throws Exception{
//		
//		
//		/*create (temp) output file*/
//		File tempOut = new File( configuration.getString( BLAST_OUTPUT_FILE ) );
//		if( ! (tempOut.createNewFile() || tempOut.canWrite() ) ){
//			throw new IOException("[BlastRunner.processSettings()]: can not write to blast output file");
//		}
//		blastConfiguration.setTempOutput( tempOut );
//		blastConfiguration.setKeepBlastOutput( configuration.getBoolean(KEEP_TEMP) );
//
//		/*input data source processing*/
//		File inputFile = null;
//		if( configuration.getString(INPUT_TYPE).equals("file") ){
//			/*read from file*/
//			inputFile = new File( configuration.getString( INPUT_FILE ) );
//			if( ! inputFile.canRead() ){
//				throw new IOException("[BlastRunner.processSettings()] can not read from query input file");
//			}
//		}
//		else if( configuration.getString(INPUT_TYPE).equals("database") ){
//			throw new Exception("this option: " + configuration.getString(INPUT_TYPE) + " not supported yet");
//		}
//		if( inputFile == null ){
//			throw new Exception("[BlastRunner.processSettings()] loading query input resource failed");
//		}
//		blastConfiguration.setInputQueries( inputFile );
//
//		/*set the output format and output file*/
//		File out = new File( configuration.getString( REPORT_FILE ) );
//		/*delete it if it already exists*/
//		out.delete();
//		
//		blastConfiguration.setOutputFormat( configuration.getString(REPORT_FORMAT) );
//		if( blastConfiguration.getOutputFormat().equalsIgnoreCase("csv") 
//				&& configuration.getBoolean(GENERATE_CSV_FILE_HEADER) ){
//			blastConfiguration.setGenerateHeaderLine(true);
//		}
//		blastConfiguration.setFilteredOutput(out);
//		
//		/*check whether a list of no-hits should be generated and create file*/
//		if( configuration.getBoolean( GENERATE_NO_HIT_LIST ) ){
//			blastConfiguration.setGenerateNoHitsList( true );
//			File noHitsFile = new File( configuration.getString(NO_HIT_FILE) );
//			/*delete it if it already exists*/
//			noHitsFile.delete();
//			blastConfiguration.setNoHitsFile( noHitsFile );
//		}
//		
//		/*setting for maximum number of hsps to keep*/
//		int maxHsps = configuration.getInt(BLAST_MAX_HSPS, Integer.MAX_VALUE);
//		blastConfiguration.setMaxHspNumber(maxHsps);
//		
//		/*store the blast program as static variable on BlastHsp*/
//		String bpStr = configuration.getString(BLAST_PROGRAM).toUpperCase();
//		BlastProgram blastProgram =  BlastProgram.valueOf(bpStr);
//		BlastHsp.setBlastProgram( blastProgram );
//		
//		/*create and set the blast command*/
//		blastConfiguration.setBlastCommand( createBlastCommand() );
//		logger.info("generated blast command: >>>" + blastConfiguration.getBlastCommand() + "<<<");
//		
//		/*create HspProperties filter*/
//		HspPropertiesFilter propFilter = createHspFilter();
//		logger.info( "created filter:\n" + propFilter.toString() );
//		blastConfiguration.setHspFilter( propFilter );
//		
//		/*parse output fields*/
//		ArrayList<HspProperty> outputFields = createOutputFieldsList();
//		StringBuilder opf = new StringBuilder();
//		opf.append( " [ " );
//		for( HspProperty prop : outputFields ){
//			opf.append( prop.name() );
//			opf.append( "; " );
//		}
//		opf.delete(opf.length() - 2, opf.length() );
//		opf.append( " ] " );
//		logger.info( "these fields were selected for output: " + opf.toString() );
//		blastConfiguration.setOutputFields( outputFields );
//		
//	}

	/**
	 * processes the settings and generates a BlastFilter object
	 */
	private void processSettings() throws Exception{
		
		/*create (temp) output file*/
		File tempOut = new File( configuration.getString( BLAST_OUTPUT_FILE ) );
		if( ! (tempOut.createNewFile() || tempOut.canWrite() ) ){
			throw new IOException("[BlastRunner.processSettings()]: can not write to blast output file");
		}
		blastConfiguration.setTempOutput( tempOut );
		blastConfiguration.setKeepBlastOutput( configuration.getBoolean(KEEP_TEMP) );

		/*input data source processing*/
		File inputFile = null;
		if( configuration.getString(INPUT_TYPE).equals("file") ){
			/*read from file*/
			inputFile = new File( configuration.getString( INPUT_FILE ) );
			if( ! inputFile.canRead() ){
				throw new IOException("[BlastRunner.processSettings()] can not read from query input file");
			}
		}
		else if( configuration.getString(INPUT_TYPE).equals("database") ){
			throw new Exception("this option: " + configuration.getString(INPUT_TYPE) + " not supported yet");
		}
		if( inputFile == null ){
			throw new Exception("[BlastRunner.processSettings()] loading query input resource failed");
		}
		blastConfiguration.setInputQueries( inputFile );

		/*set the output format and output file*/
		File out = new File( configuration.getString( REPORT_FILE ) );
		/*delete it if it already exists*/
		out.delete();
		
		blastConfiguration.setOutputFormat( configuration.getString(REPORT_FORMAT) );
		if( blastConfiguration.getOutputFormat().equalsIgnoreCase("csv") 
				&& configuration.getBoolean(GENERATE_CSV_FILE_HEADER) ){
			blastConfiguration.setGenerateHeaderLine(true);
		}
		blastConfiguration.setFilteredOutput(out);
		
		/*check whether a list of no-hits should be generated and create file*/
		if( configuration.getBoolean( GENERATE_NO_HIT_LIST ) ){
			blastConfiguration.setGenerateNoHitsList( true );
			File noHitsFile = new File( configuration.getString(NO_HIT_FILE) );
			/*delete it if it already exists*/
			noHitsFile.delete();
			blastConfiguration.setNoHitsFile( noHitsFile );
		}
		
		/*setting for maximum number of hsps to keep*/
		int maxHsps = configuration.getInt(BLAST_MAX_HSPS, Integer.MAX_VALUE);
		blastConfiguration.setMaxHspNumber(maxHsps);
		
		/*store the blast program as static variable on BlastHsp*/
		String bpStr = configuration.getString(BLAST_PROGRAM).toUpperCase();
		BlastProgram blastProgram =  BlastProgram.valueOf(bpStr);
		BlastHsp.setBlastProgram( blastProgram );
		
		
		
		/*create and set the blast command*/
		blastConfiguration.setBlastCommand( createBlastCommand() );
		logger.info("generated blast command: >>>" + blastConfiguration.getBlastCommand() + "<<<");
		
		/*create HspProperties filter*/
		HspPropertiesFilter propFilter = createHspFilter();
		logger.info( "created filter:\n" + propFilter.toString() );
		blastConfiguration.setHspFilter( propFilter );
		
		/*parse output fields*/
		ArrayList<HspProperty> outputFields = createOutputFieldsList();
		StringBuilder opf = new StringBuilder();
		opf.append( " [ " );
		for( HspProperty prop : outputFields ){
			opf.append( prop.name() );
			opf.append( "; " );
		}
		opf.delete(opf.length() - 2, opf.length() );
		opf.append( " ] " );
		logger.info( "these fields were selected for output: " + opf.toString() );
		blastConfiguration.setOutputFields( outputFields );
		
	}
	
	/**
	 * creates a list of output fields to be written to the output file (or databse).
	 * @return list of properties to output
	 * @throws Exception 
	 */
	private ArrayList<HspProperty> createOutputFieldsList() throws Exception{
		ArrayList<HspProperty> outputFields = new ArrayList<HspProperty>();

		Document doc = configuration.getDocument();
		NodeList fields = doc.getElementsByTagName("report_field");
		for( int i=0; fields.item(i) != null; i++ ){
			Node fieldNode = fields.item(i);
			String field = fieldNode.getTextContent();
			
			try{
				HspProperty property = HspProperty.valueOf( field );
				outputFields.add(property);
				//System.out.println("output field: " + field + " property: " + property.toString());
			}
			catch (Exception e) {
				throw new Exception( "[BlastRunner.createOutputFieldsList()] unknown report_field specified in configuration.xml: " + field + "; cause of exception:" + e.getCause() );
			}
		}
		return outputFields;
	}
	
	/**
	 * create a HspPropertiesFilter object from configuration settings 
	 * @return hspPropertiesFilter
	 */
	private HspPropertiesFilter createHspFilter() throws Exception {
		HspPropertiesFilter hspPropertiesFilter = new HspPropertiesFilter();
		
		Document doc = configuration.getDocument();
		NodeList filters = doc.getElementsByTagName("filter");
		for( int i=0; filters.item(i) != null; i++ ){
			Node filter = filters.item(i);
			
			HspProperty hspProperty = null;
			double filterValue = Double.MAX_VALUE;
			String type = null;
			
			NodeList filterProperties = filter.getChildNodes();
			for( int j=0; filterProperties.item(j) != null; j++ ){
				Node property = filterProperties.item(j);
				
				if(property.getNodeName().equals("filter_property") ){
					hspProperty = HspProperty.valueOf(property.getTextContent());
				}
				if(property.getNodeName().equals("filter_value") ){
					filterValue = Double.parseDouble( property.getTextContent() );
				}
				if(property.getNodeName().equals("filter_type") ){
					type = property.getTextContent();
				}
			}

			if( hspProperty==null || type==null || filterValue==Double.MAX_VALUE){
				logger.fatal("filter parsing process failed; aborting");
				throw new Exception("[BlastRunner.createHspFilter()]: filter parsing process failed");
			}
			
			if(type.equalsIgnoreCase("minimum")){
				hspPropertiesFilter.setMinimumPropertyValue(hspProperty, filterValue);
			}
			else if(type.equalsIgnoreCase("maximum")){
				hspPropertiesFilter.setMaximumPropertyValue(hspProperty, filterValue);
			}
			else{
				throw new Exception( "[BlastRunner.createHspFilter()] filter parsing process failed: unknown filter type: " + type );
			}
		}
		//System.out.println( hspPropertiesFilter.toString() );
		return hspPropertiesFilter;
	}

	/**
	 * create the blast command from configuration settings
	 * @return blast command
	 */
	private String createBlastCommand() {
		StringBuilder command = new StringBuilder();
		command.append( configuration.getString(BLAST_EXECUTABLE));
		command.append(" -p ");
		command.append( configuration.getString(BLAST_PROGRAM) );
		command.append(" -d ");
		command.append( configuration.getString(BLAST_DB) );
		command.append(" -i ");
		command.append( blastConfiguration.getInputSequenceFile().getAbsolutePath() );
		command.append(" -o ");
		command.append( blastConfiguration.getTempOutputFile().getAbsolutePath() );
		command.append(" ");
		if( configuration.getString(BLAST_ARGS) != null ){
			command.append( configuration.getString(BLAST_ARGS) );
		}
		command.append(" -m 7");
		return command.toString();
	}

	/**
	 * log the start of the analysis procedure
	 * @throws IOException 
	 */
	private void logAnalysisStart() throws IOException{
		//PropertyConfigurator.configure( configuration.getString(LOG_CONFIGFILE) );
		if (logger == null) { // create logger if it does not exists.
		    try {
			    Layout layout = new PatternLayout("[%5p] %d{yyyy-MM-dd} %d{HH:mm:ss} %c (%F:%M:%L)%n%m%n%n");
				Appender appender = new FileAppender(layout, configuration.getString(LOG_FILE) );
				BasicConfigurator.configure( appender );
			    logger = Logger.getLogger( configuration.getString(LOGGER_NAME) );
				logger.setLevel( Level.toLevel( configuration.getString(LOG_LEVEL) ) );
			} catch (IOException e) {
				throw new IOException("[BlastRunner.logAnalysisStart()]: unable to configure logger" + "; cause of exception:" + e.getCause());
				//e.printStackTrace();
			} 
		}
		logger.info("starting analysis with the following settings" 
				+ "\nblast program: " + configuration.getString(BLAST_PROGRAM) 
				+ "\nblast database: " + configuration.getString(BLAST_DB) 
				+ "\nblast arguments: " + configuration.getString(BLAST_ARGS) 
				+ "\nblast input type: " + configuration.getString(INPUT_TYPE)
				+ "\nkeep blast xml output: " + configuration.getString(KEEP_TEMP) 
				+ "\nreport format: " + configuration.getString(REPORT_FORMAT) 
				+ "\nreport file: " + configuration.getString(REPORT_FILE) );
		
/*		if( configuration.getList(HSP_FILTERS) != null ){
			 filters = configuration.getList(HSP_FILTERS);
		}
*/		
		
		if( configuration.getString(INPUT_TYPE).equals("file") ){
			logger.info("blast input file: " + configuration.getString(INPUT_FILE) );
		}
		else if( configuration.getString(INPUT_TYPE).equals("database") ){
			logger.info("database with query sequences: " + configuration.getString(DB_NAME) 
					+ " at " + configuration.getString(DB_HOST) 
					+ "\ndatabase user: " + configuration.getString(DB_USER)
					+ "\ndatabase sequence query: " + configuration.getString(DB_QUERY));
		}
		
		if( configuration.getBoolean( KEEP_TEMP ) ){
			logger.info("saving raw blast output to file: " + configuration.getString( BLAST_OUTPUT_FILE ) );
		}
	}
	
    /**
     * The loadConfiguration method loads the configuration for the application from file.
     * If this configuration cannot be loaded, a ConfigurationException 
     * will be thrown.
     * It will also start the logger if it has yet been started
     * @throws Exception
     */
    private void loadConfiguration( ) throws Exception {
        try {
            File xmlConfigFile = new File( configFile );
            if (! xmlConfigFile.canRead() ) {
                throw new Exception( "unable to read configuration file: " + configFile);
            }
            configuration = new XMLConfiguration( xmlConfigFile );
        } catch (Exception e) { // configuration could not be loaded
			throw new Exception("[BlastRunner.loadConfiguration()] unable to load configuration file: " + configFile + "; cause of exception:" + e.getCause() );
        }
    }


}
