package net.cellingo.sequence_tools.blast;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;

public class BlastRunnerCL {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BlastRunnerCL brcl = new BlastRunnerCL();
		brcl.start( args );
	}

	
	private void start(String[] args) {
		Options o = createOptions();
		CommandLine cmd  = null;
		try {
			cmd = fetchOptions(o, args);
			BlastRunner br = new BlastRunner(cmd.getOptionValue("c"));
			br.startCLmode(
					cmd.getOptionValue("e"),
					cmd.getOptionValue("d"),
					cmd.getOptionValue("i"),
					cmd.getOptionValue("o"),
					cmd.getOptionValue("r"),
					cmd.getOptionValue("n"),
					cmd.getOptionValue("l"));
			
		} catch (ParseException e) {
			System.out.println( "ERROR! " + e.getMessage() );
			
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "Blastrunner", o );
		}

	}
	
	private CommandLine fetchOptions(Options options, String[] args) throws ParseException {
		CommandLineParser parser = new PosixParser(); //GnuParser();//
		CommandLine cmd = parser.parse( options, args);
		boolean verbose = true;
		
		String e = cmd.getOptionValue("e");
		if(e == null) throw new ParseException("missing option: " + options.getOption("e"));
		else if(verbose) System.out.println("option \"blast executable\": " + e);

		String d = cmd.getOptionValue("d");
		if(d == null) throw new ParseException("missing option: " + options.getOption("d"));
		else if(verbose) System.out.println("option \"database\": " + d);

		String i = cmd.getOptionValue("i");
		if(i == null) throw new ParseException("missing option: " + options.getOption("i"));
		else if(verbose) System.out.println("option \"input sequence file\": " + i);

		String o = cmd.getOptionValue("o");
		if(o == null) throw new ParseException("missing option: " + options.getOption("o"));
		else if(verbose) System.out.println("option \"blast executable output file\": " + o);

		String r = cmd.getOptionValue("r");
		if(r == null) throw new ParseException("missing option: " + options.getOption("r"));
		else if(verbose) System.out.println("option \"blastrunner output file\": " + r);

		String n = cmd.getOptionValue("n");
		if(n == null) throw new ParseException("missing option: " + options.getOption("n"));
		else if(verbose) System.out.println("option \"blastrunner nohits file\": " + n);

		String l = cmd.getOptionValue("l");
		if(l == null) throw new ParseException("missing option: " + options.getOption("l"));
		else if(verbose) System.out.println("option \"blastrunner log file\": " + l);
		
		String c = cmd.getOptionValue("c");
		if(c == null) throw new ParseException("missing option: " + options.getOption("c"));
		else if(verbose) System.out.println("option \"blastrunner config file\": " + c);
		
		return cmd;
		
}


	private Options createOptions(){
		// create Options object
		Options options = new Options();
		options.addOption("e", true, "blast executable to run");
		options.addOption("d", true, "blast database to use");
		options.addOption("i", true, "input sequence file");
		options.addOption("o", true, "blast executable output file");
		options.addOption("r", true, "blastrunner output file");
		options.addOption("n", true, "blastrunner nohits file");
		options.addOption("l", true, "blastrunner log file");
		options.addOption("c", true, "blastrunner config file - XML file containing other configuration settings");
		return options;
	}

}
