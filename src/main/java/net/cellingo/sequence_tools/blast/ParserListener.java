/**
 * 
 */
package net.cellingo.sequence_tools.blast;

/**
 * This interface must be implemented by classes that want to process blast results 
 * in a streaming manner. It defines only a single method: iterationReady(), to be 
 * called when a set of results for a single query is ready.
 * @author Cellingo
 * @version 1.0
 */
public interface ParserListener {
	/**
	 * This method will be called by the BlastParser when a set of results for a single query is ready.
	 * @param query
	 */
	public void iterationReady( BlastQuery query );
}
