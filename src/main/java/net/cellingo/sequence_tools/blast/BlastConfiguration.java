/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.io.File;
import java.util.List;

/**
 * This class encapsulates all configurations and settings 
 * for running and filetring blast results
 * @author Cellingo
 * @version 1.0
 */
public class BlastConfiguration {
	public static final String FILETYPE_XML = "xml";
	public static final String FILETYPE_CSV = "csv";
	
	private String blastCommand;
	private HspPropertiesFilter hspFilter;
	private File inputQueries;
	private File tempOutput;
	private File filteredOutput;
	private File noHitsFile;
	private String outputFormat;
	private boolean keepBlastOutput;
	private boolean generateNoHitsList;
	private List<HspProperty> outputFields;
	private boolean generateHeaderLine;
	private int maxHspNumber;
	
	/**
	 * set whether the blast output should be kept; by default
	 * this file will be removed after processing
	 * @param keepBlastOutput
	 */
	public void setKeepBlastOutput(boolean keepBlastOutput) {
		this.keepBlastOutput = keepBlastOutput;
	}
	/**
	 * returns whether the temporary BLAST output should be kept
	 * @return keep blast output
	 */
	public boolean isKeepBlastOutput() {
		return keepBlastOutput;
	}
	/**
	 * @return the outputFormat
	 */
	public String getOutputFormat() {
		return outputFormat;
	}
	/**
	 * sets the output format. Valid values specified as public final String values in this class.
	 * @param outputFormat the outputFormat to set
	 */
	public void setOutputFormat(String outputFormat) throws IllegalArgumentException {
		if( ! (outputFormat.equalsIgnoreCase(FILETYPE_CSV) || outputFormat.equalsIgnoreCase(FILETYPE_XML) ) ){
			throw new IllegalArgumentException("requested output format not supported");
		}
		this.outputFormat = outputFormat;
	}
	/**
	 * @return the blastCommand
	 */
	public String getBlastCommand() {
		return blastCommand;
	}
	/**
	 * @param blastCommand the blastCommand to set
	 */
	public void setBlastCommand(String blastCommand) {
		this.blastCommand = blastCommand;
	}
	/**
	 * @return the hspFilter
	 */
	public HspPropertiesFilter getHspFilter() {
		return hspFilter;
	}
	/**
	 * @param hspFilter the hspFilter to set
	 */
	public void setHspFilter(HspPropertiesFilter hspFilter) {
		this.hspFilter = hspFilter;
	}
	/**
	 * @param maxHspNumber the maxHspNumber to set
	 */
	public void setMaxHspNumber(int maxHspNumber) {
		this.maxHspNumber = maxHspNumber;
	}
	/**
	 * @return the maxHspNumber
	 */
	public int getMaxHspNumber() {
		return maxHspNumber;
	}
	/**
	 * @return the inputQueries
	 */
	public File getInputSequenceFile() {
		return inputQueries;
	}
	/**
	 * @param inputQueries the inputQueries to set
	 */
	public void setInputQueries(File inputQueries) {
		this.inputQueries = inputQueries;
	}
	/**
	 * @return the tempOutput
	 */
	public File getTempOutputFile() {
		return tempOutput;
	}
	/**
	 * @param tempOutput the tempOutput to set
	 */
	public void setTempOutput(File tempOutput) {
		this.tempOutput = tempOutput;
	}
	/**
	 * @return the filteredOutput
	 */
	public File getFilteredOutputFile() {
		return filteredOutput;
	}
	/**
	 * @param filteredOutput the filteredOutput to set
	 */
	public void setFilteredOutput(File filteredOutput) {
		this.filteredOutput = filteredOutput;
	}
	/**
	 * set the file to which a list of sequence IDs should be written
	 * of sequences without a valid blast hit 
	 * @param noHitsFile
	 */
	public void setNoHitsFile(File noHitsFile) {
		this.noHitsFile = noHitsFile;
	}
	/**
	 * returns the file where the sequence descriptions should be written 
	 * to that have no valid blast hit 
	 * @return noHit File
	 */
	public File getNoHitsFile(){
		return noHitsFile;
	}
	/**
	 * returns whether a noHit list should be generated
	 * @return generate noHitsList
	 */
	public boolean isGenerateNoHitsList() {
		return generateNoHitsList;
	}
	/**
	 * specify whether a noHit list should be generated
	 * @param generateNoHitsList the generateNoHitsList to set
	 */
	public void setGenerateNoHitsList(boolean generateNoHitsList) {
		this.generateNoHitsList = generateNoHitsList;
	}
	/**
	 * set a list of output fields
	 * @param outputFields
	 */
	public void setOutputFields(List<HspProperty> outputFields) {
		this.outputFields = outputFields;
	}
	/**
	 * returns a list of output fields to write
	 * @return output fields
	 */
	public List<HspProperty> getOutputFields(){
		return outputFields;
	}
	/**
	 * specify whether a header line should be created in the output csv file
	 * @param generateHeader
	 */
	public void setGenerateHeaderLine(boolean generateHeader) {
		this.generateHeaderLine = generateHeader;
	}
	/**
	 * returns whether a header line should be created in the output csv file
	 * @return generateHeaderLine
	 */
	public boolean isGenerateHeaderLine(){
		return generateHeaderLine;
	}
	
	
}
