package net.cellingo.sequence_tools.blast;

/**
 * the five possible blast programs
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 1.0
 */
public enum BlastProgram {
	BLASTN("nucleotide query against nucleotide database"),
	BLASTP("protein query against protein database"),
	BLASTX("translated nucleotide query against protein database"),
	TBLASTN("protein query against translated nucleotide database"),
	TBLASTX("translated nucleotide query against translated nucleotide database");
	
	private String type;

	private BlastProgram(String type){
		this.type = type;
	}
	
	public String tostring(){
		return type;
	}
}
