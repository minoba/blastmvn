/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.io.File;

//import com.sun.xml.internal.ws.streaming.XMLReaderException;

/**
 * This interface defines the methods all blast parsers should implement
 * @author Cellingo
 * @version 1.0
 */
public interface BlastParser {
	/**
	 * sets the file to be parsed
	 * @param file
	 */
	public void setFile( File file );
	/**
	 * sets the maximum number of HSPs to process for each query
	 * @param maxHsps the maxHsps to set
	 */
	public void setMaxHsps(int maxHsps);
	/**
	 * starts the parsing process
	 * @throws XMLReaderException
	 */
	public void parse() throws Exception;
	/**
	 * Returns a BlastResults object that encapsulates all results and 
	 * provides several methods for querying them
	 */
	public BlastResults getBlastResults();
	/**
	 * Provide an object to write the blast results to. This object may be initialized with 
	 * a list of queryIDs that were send to the blast process. If none was provided, a new 
	 * one will be created. If one already existed, it will be overwritten.
	 * @param results
	 */
	public void setBlastResults( BlastResults results );
	/**
	 * perform the parsing process streaming and report to the provided listener
	 * every processed Iteration, i.e.: query result.
	 * The listener will have to implement the ParserListener interface which defines
	 * one single method: iterationReady( String queryDef, List<BlastHit> hitList ),
	 * where queryDef is the definition of the analyzed query and List<BlastHit> hitList
	 * the results of the query
	 * @param listener to the parsing process
	 */
	public void parseStreaming( ParserListener listener );
}
