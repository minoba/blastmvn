package net.cellingo.sequence_tools.blast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * inner class to encapsulate some simple query data:
 * order in which it is entered, queryID
 * @author michiel
 */
public class BlastQuery implements Comparable<BlastQuery>{
	public static final int STATUS_NO_HIT = 0; 
	public static final int STATUS_WITH_HIT = 1;
	private static int queryCount;
	private int iterationNumber;
	private int queryNumber;
	//private int hspNumber;
	private int hitStatus;
	private String queryId;
	private HashMap<Integer, BlastHit> blastHits;
	private int queryLength;
	
	/**
	 * bean constructor
	 */
	public BlastQuery(){
		init();
	}
	/**
	 * construct with ID
	 * @param queryID
	 */
	public BlastQuery( String queryID ){
		init();
		this.queryId = queryID;
	}
	/**
	 * construct with ID and status (hit/no-hit)
	 * @param queryID
	 * @param status
	 */
/*	public BlastQuery( String queryID, int status ){
		init();
		this.queryId = queryID;
		this.hitStatus = status;
	}
*/	
	private void init(){
		queryCount++;
		this.queryNumber = queryCount;
		this.hitStatus = STATUS_NO_HIT;
	}

	/**
	 * remove the given hit from this query. If it does not exist, nothing happens.
	 * @param hit
	 */
	public void removeHit(BlastHit hit) {
		blastHits.remove( hit.getHitNumber() );
		if( blastHits.size() == 0 ) this.hitStatus = STATUS_NO_HIT;
	}

	/**
	 * @return the iterationNumber
	 */
	public int getIterationNumber() {
		return iterationNumber;
	}
	/**
	 * @param iterationNumber the iterationNumber to set
	 */
	public void setIterationNumber(int iterationNumber) {
		this.iterationNumber = iterationNumber;
	}
	/**
	 * Returns an iterator for the blast hits associated with this query 
	 * @return blasthit iterator
	 */
	public Iterator<BlastHit> getBlastHits( ){
		List<BlastHit> list = new ArrayList<BlastHit>();
		if( blastHits != null ){
			list.addAll( blastHits.values() );
			Collections.sort( list );
		}
		return list.iterator();
	}
	/**
	 * @return the hitNumber
	 */
	public int getHitNumber() {
		if( blastHits == null ) return 0;
		return blastHits.size();
	}
	/**
	 * add a blast hit to this query
	 * @param hit
	 */
	public void addBlastHit( BlastHit hit ){
		if(blastHits==null) blastHits = new HashMap<Integer, BlastHit>();
		blastHits.put(hit.getHitNumber(), hit);
	}
	/**
	 * @return the queryStatus
	 */
	public int getHitStatus() {
		if( this.blastHits!=null && this.blastHits.size() == 0 && this.hitStatus == BlastQuery.STATUS_WITH_HIT ){
			//this means all hits have been removed
			this.setHitStatus( BlastQuery.STATUS_NO_HIT );
		}
		return hitStatus;
	}
	/**
	 * @param hitStatus the queryStatus to set
	 */
	public void setHitStatus(int hitStatus) {
		this.hitStatus = hitStatus;
	}
	/**
	 * returns whether this query has blast hits
	 * @return presence of hits
	 */
	public boolean hasHits(){
		if( hitStatus == STATUS_WITH_HIT ) return true;
		return false;
	}
	
	/**
	 * @return the hspNumber
	 */
	public int getHspNumber() {
		if( blastHits == null ) return 0;
		int number = 0;
		for(BlastHit hit : blastHits.values()){
			number += hit.getHspNumber();
		}
		return number;
	}
	
	/**
	 * @return the queryNumber
	 */
	public int getQueryNumber() {
		return queryNumber;
	}
	/**
	 * @return the queryId
	 */
	public String getQueryId() {
		return queryId;
	}
	/**
	 * @param queryId the queryId to set
	 */
	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}
	/**
	 * sets the length of this query 
	 * @param queryLength
	 */
	public void setQueryLength(int queryLength) {
		this.queryLength = queryLength;
	}
	/**
	 * returns the length of this query
	 * @return query length
	 */
	public int getQueryLength(){
		return queryLength;
	}
	
	//@Override
	public int compareTo(BlastQuery otherQuery) {
		return this.getQueryNumber() - otherQuery.getQueryNumber();
	}
		
}
