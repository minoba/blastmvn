/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.util.HashMap;
import java.util.HashSet;

import net.cellingo.utils.conversion.ValueType;

/**
 * This enum defines all possible blast HSP properties, both those initially
 * determined by BLAST and also derivative ones 
 * @author Cellingo
 * @version 1.0
 */
public enum HspProperty {
	QUERY_LENGTH("lenght query seq"),
	HIT_ID("hit ID"),
	HIT_ACCESSION("hit accession"),
	HIT_DEFINITION("hit definition"),
	HIT_LENGTH("hit length"),
	
	HSP_NUMBER("hsp number"),
	HSP_BIT_SCORE("bit score"),
	HSP_SCORE("score"),
	HSP_EVALUE("evalue"),
	HSP_QUERY_FROM("query-from"),
	HSP_QUERY_TO("query-to"),
	HSP_HIT_FROM("hit-from"),
	HSP_HIT_TO("hit-to"),
	HSP_QUERY_FRAME("query frame"),
	HSP_HIT_FRAME("hit frame"),
	HSP_IDENTITIES("identity"),
	HSP_POSITIVES("positive"),
	HSP_ALIGN_LENGTH("align length"),
	HSP_QUERY_SEQUENCE("query seq"),
	HSP_HIT_SEQUENCE("hit seq"),
	HSP_MIDLINE("midline"),
	
	HSP_IDENTITY_PERCENTAGE("percent identical characters"),
	HSP_POSITIVE_PERCENTAGE("percent positive characters"),
	HSP_MISMATCH_NUMBER("number mismatching characters"),
	HSP_ALIGN_PERCENTAGE("percent of query length in alignment"),
	HSP_MISMATCH_PERCENTAGE("percent mismatching characters"),
	HSP_GAP_OPENINGS("gap opening positions");
	
	private static HashMap<HspProperty, ValueType> valueTypes;
	private static HashSet<HspProperty> derivedProperties;
	
	static{
		derivedProperties = new HashSet<HspProperty>();
		derivedProperties.add(HspProperty.HSP_IDENTITY_PERCENTAGE);
		derivedProperties.add(HspProperty.HSP_POSITIVE_PERCENTAGE);
		derivedProperties.add(HspProperty.HSP_MISMATCH_NUMBER);
		derivedProperties.add(HspProperty.HSP_ALIGN_PERCENTAGE);
		derivedProperties.add(HspProperty.HSP_MISMATCH_PERCENTAGE);
		derivedProperties.add(HspProperty.HSP_GAP_OPENINGS);
		
		valueTypes = new HashMap<HspProperty, ValueType>();
		
		valueTypes.put( HspProperty.HIT_ID, ValueType.STRING );
		valueTypes.put( HspProperty.HIT_DEFINITION, ValueType.STRING );
		valueTypes.put( HspProperty.HIT_ACCESSION, ValueType.STRING );
		
		valueTypes.put( HspProperty.QUERY_LENGTH, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_NUMBER, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_BIT_SCORE, ValueType.DOUBLE );
		valueTypes.put( HspProperty.HSP_SCORE, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_EVALUE, ValueType.DOUBLE );
		valueTypes.put( HspProperty.HSP_QUERY_FROM, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_QUERY_TO, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_HIT_FROM, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_HIT_TO, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_QUERY_FRAME, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_HIT_FRAME, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_IDENTITIES, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_POSITIVES, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_ALIGN_LENGTH, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_QUERY_SEQUENCE, ValueType.STRING );
		valueTypes.put( HspProperty.HSP_HIT_SEQUENCE, ValueType.STRING );
		valueTypes.put( HspProperty.HSP_MIDLINE, ValueType.STRING );
		
		valueTypes.put( HspProperty.HSP_IDENTITY_PERCENTAGE, ValueType.DOUBLE );
		valueTypes.put( HspProperty.HSP_POSITIVE_PERCENTAGE, ValueType.DOUBLE );
		valueTypes.put( HspProperty.HSP_MISMATCH_NUMBER, ValueType.INTEGER );
		valueTypes.put( HspProperty.HSP_ALIGN_PERCENTAGE, ValueType.DOUBLE );
		valueTypes.put( HspProperty.HSP_MISMATCH_PERCENTAGE, ValueType.DOUBLE );
		valueTypes.put( HspProperty.HSP_GAP_OPENINGS, ValueType.INTEGER );
	}
	
	private String type;
	
	private HspProperty( String type ){
		this.type = type;
	}
	
	/**
	 * returns whether the argument is a derived property
	 * @param hspProperty
	 * @return property is derived
	 */
	public static boolean isDerivedProperty( HspProperty hspProperty ){
		if( derivedProperties.contains(hspProperty) ) return true;
		return false;
	}
	
	/**
	 * get the type of value for this property
	 * @param hspProperty
	 * @return the value type of the property
	 */
	public static ValueType getValueType( HspProperty hspProperty ){
		if(valueTypes.containsKey( hspProperty )){
			return valueTypes.get( hspProperty );
		}
		return ValueType.UNKNOWN;
	}
	
	public String toString(){
		return type;
	}
}
