/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class encapsulates a BLAST hit, which can consist of multiple HSPs,
 * always from a single query, on a database sequence.
 * @author Cellingo
 * @version 1.0
 */
public class BlastHit implements Comparable<BlastHit>{
	private static int hitCount;
	private static Pattern organismExtractionPattern;
	private int hitNumber;
	//private String queryID;
	private String hitID;
	private String hitAccession;
	private String hitDefinition;
	private String hitDescription;
	private String organism;
	private int hitLength;
	private HashMap<Integer, BlastHsp> hsps;

	/**
	 * bean constructor
	 */
	public BlastHit(){
		init();
	}
	
	/**
	 * construct with a hit ID ( the "<Hit-id>" XML field )
	 * @param hitID
	 */
	public BlastHit( String hitID ){
		init();
		this.hitID = hitID;
	}
	
	/**
	 * init code
	 */
	private void init(  ){
		hitCount++;
		this.hitNumber = hitCount;
		hsps = new HashMap<Integer, BlastHsp>();
	}
	
	/**
	 * add an HSP to the blast hit
	 * @param hsp
	 */
	public void addHsp( BlastHsp hsp ){
		hsps.put(hsp.getHspNumber(), hsp);
	}
	
	/**
	 * get an iterator of the hsps in this blast hit
	 * @return hsp iterator
	 */
	public Iterator<BlastHsp> getHsps(){
		List<BlastHsp> list = new ArrayList<BlastHsp>();
		list.addAll( hsps.values() );
		Collections.sort( list );
		return list.iterator();
	}
	
	/**
	 * convenience method: get the first Hsp associated with this blast hit
	 * @return first hsp
	 */
	public BlastHsp getFirstHsp(){
		if( hsps.size() > 0 ){
			List<Integer> keys = new ArrayList<Integer>();
			keys.addAll( hsps.keySet() );
			Collections.sort( keys );
			return hsps.get( keys.get(0) );
		}
		else return null; 
	}

	/**
	 * convenience method: get the best scoring (on property: e-value) Hsp associated with this blast hit
	 * if no e-value property is present, the list will remain unsorted. If this method is called, the HSPs
	 *  in this collection will stay ordered according to e value 
	 * @return best hsp
	 */
	public BlastHsp getBestHsp(){
		List<BlastHsp> list = new ArrayList<BlastHsp>();
		list.addAll( hsps.values() );
		Collections.sort( list );
		return list.get(0);
	}

	/**
	 * returns the hit serial number of this hit;
	 * @return hit serial number
	 */
	public int getHitNumber(){
		return hitNumber;
	}

	/**
	 * returns the number of HSPs in this hit
	 * @return HSPnumber
	 */
	public int getHspNumber(){
		if(hsps == null) return 0;
		return hsps.size();
	}
	
	/**
	 * @return the hitID
	 */
	public String getHitID() throws NoSuchFieldException{
		if( this.hitID == null ) throw new NoSuchFieldException("requested field hitID does not exist");
		return hitID;
	}

	/**
	 * @param hitID the hitID to set
	 */
	public void setHitID(String hitID) {
		this.hitID = hitID;
	}

	/**
	 * @return the hitAccession
	 */
	public String getHitAccession() throws NoSuchFieldException{
		if( this.hitAccession == null ) throw new NoSuchFieldException("requested field hitAccession does not exist");
		return hitAccession;
	}

	/**
	 * @param hitAccession the hitAccession to set
	 */
	public void setHitAccession(String hitAccession) {
		this.hitAccession = hitAccession;
	}

	/**
	 * @return the hitDefinition
	 */
	public String getHitDefinition() throws NoSuchFieldException{
		if( this.hitDefinition == null ) throw new NoSuchFieldException("requested field hitDefinition does not exist");
		return hitDefinition;
	}

	/**
	 * @param hitDefinition the hitDefinition to set
	 */
	public void setHitDefinition(String hitDefinition) {
		this.hitDefinition = hitDefinition;
	}

	/**
	 * @return the hitDescription
	 */
	public String getHitDescription() throws NoSuchFieldException{
		if( this.hitDescription == null ) throw new NoSuchFieldException("requested field hitDescription does not exist");
		return hitDescription;
	}

	/**
	 * @param hitDescription the hitDescription to set
	 */
	public void setHitDescription(String hitDescription) {
		this.hitDescription = hitDescription;
	}

	/**
	 * @return the organism
	 */
	public String getOrganism() throws NoSuchFieldException{
		if( this.organism == null ){
			/*try parsing it right now from the hit definition
			 *putative alcohol dehydrogenase [Prevotella copri DSM 18205]*/
			if( organismExtractionPattern == null ){
				/*look for a string of non-[ characters, as short as possoble, at the end of the line*/
				String pattern = "\\[([^\\[]+?)\\]$";
				BlastHit.organismExtractionPattern = Pattern.compile(pattern);
			}
			if( hitDefinition == null ){
				throw new NoSuchFieldException("there is no parsable hit definition field for fetching organism name");
			}
			Matcher m = organismExtractionPattern.matcher( this.hitDefinition );
			if( m.find() ){
				organism = m.group(1);
			}
			else throw new NoSuchFieldException("there is no parsable organism name in the hit definition: " + this.hitDefinition);
			
		}
		return organism;
	}

	/**
	 * @param organism the organism to set
	 */
	public void setOrganism(String organism) {
		this.organism = organism;
	}

	/**
	 * @return the hitLength
	 */
	public int getHitLength() throws NoSuchFieldException{
		if( this.hitLength == 0 ) throw new NoSuchFieldException("requested field hitLength does not exist");
		return hitLength;
	}

	/**
	 * @param hitLength the hitLength to set
	 */
	public void setHitLength(int hitLength) {
		this.hitLength = hitLength;
	}

	/**
	 * removes the HSP from this BlastHit. If it does noet exist in this object, 
	 * nothing happens
	 * @param hsp
	 */
	public void removeHsp(BlastHsp hsp) {
		hsps.remove(hsp.getHspNumber());
	}

	/**
	 * returns whether this hit (still) contains one or more HSPs
	 * @return empty status
	 */
	public boolean isEmpty(){
		if( hsps.size() == 0 ){
			return true;
		}
		return false;
	}

	/**
	 * compares according to the best HSP (according to the e-value) of each hit
	 */
	//@Override
	public int compareTo(BlastHit otherHit) {
		final int BEFORE = -1;
		final int AFTER = 1;
		final int EQUAL = 0;
		try{
			if( this.getBestHsp().compareTo( otherHit.getBestHsp() ) < 0 ){
				return BEFORE;
			}
			else if( this.getBestHsp().compareTo( otherHit.getBestHsp() ) > 0 ){
				return AFTER;
			}
			else return EQUAL;
		} catch(Exception e){
			return EQUAL;
		}
	}
	
	public static void main( String[] args ){
		BlastHit bh = new BlastHit();
		bh.setHitDefinition("putative alcohol dehydrogenase [Prevotella copri DSM 18205] >gi|281403844|gb|EFB34524.1| putative alcohol dehydrogenase [Prevotella copri DSM 18205]");
		//bh.setHitDefinition("GMP synthase, large subunit [Geobacter sp. FRC-32] >gi|254800100|sp|B9M9G4.1|GUAA_GEOSF RecName: Full=GMP synthase [glutamine-hydrolyzing]; AltName: Full=Glutamine amidotransferase; AltName: Full=GMP synthetase >gi|221564564|gb|ACM20536.1| GMP synthase, large subunit [Geobacter sp. FRC-32]");
		try {
			System.out.println( bh.getOrganism() );
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		}
		
	}
}
