/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.apache.commons.configuration.XMLConfiguration;
import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

/**
 * @author michiel
 *
 */
public class BlastWrapper {
    /**
     * XPaths to the configuration file settings
     */
    public static final String LOGGER_NAME = "log.name";
    public static final String LOG_LEVEL = "log.level";
    public static final String LOG_FILE = "log.file";
//    public static final String LOG_CONFIGFILE = "log.properties_file";

    public static final String BLAST_ARGS = "blast.blast_arguments";
    
    public static final String INPUT_FILE = "blast.input_file";
    
    public static final String BLAST_OUTPUT_FILE = "blast.blast_output_file";
    public static final String REPORT_FILE = "report_file";
    public static final String NO_HIT_FILE = "no_hit_file";

	/**
	 * the log4j logger object
	 */
	private static Logger logger;

    /**
     * the name of the config file
     */
	private String configFile;
    /**
     * The configuration settings for the database connection.
     */
    private XMLConfiguration configuration;
    /**
     * the BlastConfiguration object is created form the settings specified 
     * in the configuration xml file
     */
    //private BlastConfiguration blastConfiguration;
	private String blastCommand;
	private File nohitsFile;
	private LinkedHashMap<String, Boolean> sequenceScanStatus = new LinkedHashMap<String, Boolean>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if( args.length != 1){
			System.out.println("no configuration file provided!");
			System.out.println("usage: java -jar BlastRunner.jar <configuration file>");
			System.out.println("aborting");
			System.exit( 1 );
		}
		BlastWrapper blastRunner = new BlastWrapper( args[0] );
		blastRunner.start();

	}
	
	/**
	 * construct with the name of the config file
	 * @param configFile
	 */
	public BlastWrapper( String configFile ){
		this.configFile = configFile;
	}
	
	/**
	 * start the analysis procedure
	 */
	private void start(){
		try {
			loadConfiguration();
			
			processSettings();
			
			logger.info("generating no-hits list");
			
			createSequenceScanStatus();
			
			logger.info("generated blast command: >>>" + this.blastCommand + "<<<");

			doBlast();
		
			logger.info("generating no-hits list");
			
			generateNohitsList();
			
			logger.info("finished");
			
		} catch (Exception e) {
			e.printStackTrace();
			/*all program exceptions that occur during execution converge here*/
			logger.fatal("An error occurred. Aborting.\nThe following information is available:\n" + e.getMessage() 
					+ "\ncause:" + e.getCause().toString() 
					+ "\nstack trace:\n" + e.getStackTrace().toString() );
			System.out.println("An error occurred. Aborting. See log for details.");
			//cleanUp();
			System.exit( 1 );
		}
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	private void generateNohitsList() throws Exception{
		PrintWriter pw = new PrintWriter( configuration.getString(NO_HIT_FILE) );
		
		BufferedReader br = new BufferedReader( new FileReader(configuration.getString(BLAST_OUTPUT_FILE) ) );
		String line;
		while( (line = br.readLine()) != null ){
			//System.out.println( line );
			String description = line.substring(0, line.indexOf("\t"));
			//System.out.println( "\t::" + description + "::");
			if( sequenceScanStatus.containsKey(description) ){
				sequenceScanStatus.put(description, true);
			}
			else assert false : "Sequence with ID " + description + " was not registered for scan so it should not occur in output";
		}
		
		for( Entry<String, Boolean> entry : sequenceScanStatus.entrySet() ){
			if( ! entry.getValue() ){
				pw.println( entry.getKey() );
			}
			//System.out.println("\t\t"+entry.toString());
		}
		br.close();
		pw.close();
	}
	
	/**
	 * registers all sequences for scanning; later they will be check for having a hit
	 * @throws Exception
	 */
	private void createSequenceScanStatus() throws Exception{
		/*process input file and register sequences for scan*/
		
		//sed -i 's/ /~/g' M_marisnigri_0_error_10_reads.fa
		BufferedReader br = new BufferedReader( new FileReader(configuration.getString(INPUT_FILE) ) );
		String line;
		while( (line = br.readLine()) != null ){
			
			if( line.startsWith(">") ){
				sequenceScanStatus.put(line.substring(1), false);
			}
		}
		//System.out.println( sequenceScanStatus.toString() );
		br.close();
	}
	
	/**
	 * perfom the actual Blast analysis
	 */
	private void doBlast() throws Exception{
		logger.info("starting BLAST process");
		/**/
		String s = null;
        Process ps;
		try {
			ps = Runtime.getRuntime().exec( this.blastCommand );
	        BufferedReader psStdInput = new BufferedReader( new InputStreamReader(ps.getInputStream()) );
	        BufferedReader psStdError = new BufferedReader( new InputStreamReader(ps.getErrorStream()) );

	        /* read the output from the command */
	        while ((s = psStdInput.readLine()) != null) {
	        	logger.debug("STDOUT from BLAST: " + s);
	        }
	        /* read any errors from the attempted command */
	        if( (s = psStdError.readLine()) != null){
	        	StringBuilder sberr = new StringBuilder();
	        	sberr.append( "STDERR from BLAST: " + s + "\n" );
		        while ((s = psStdError.readLine()) != null) {
		        	sberr.append( "STDERR from BLAST: " + s + "\n" );
		        }
		        logger.error( sberr.toString() );
	        }
		} catch (IOException e) {
			//e.printStackTrace();
			throw new Exception("[" + this.getClass().getSimpleName() + ".doBlast()] BLAST run failed");
		}
		ps.getInputStream().close();
		ps.getErrorStream().close();
		logger.info("BLAST process finished");
	}

	
	/**
	 * processes the settings and generates a BlastFilter object
	 */
	private void processSettings() throws Exception{
		
		//PropertyConfigurator.configure( configuration.getString(LOG_CONFIGFILE) );
		if (logger == null) { // create logger if it does not exists.
		    try {
			    Layout layout = new PatternLayout("[%5p] %d{yyyy-MM-dd} %d{HH:mm:ss} %c (%F:%M:%L)%n%m%n%n");
				Appender appender = new FileAppender(layout, configuration.getString(LOG_FILE) );
				BasicConfigurator.configure( appender );
			    logger = Logger.getLogger( configuration.getString(LOGGER_NAME) );
				logger.setLevel( Level.toLevel( configuration.getString(LOG_LEVEL) ) );
			} catch (IOException e) {
				throw new IOException("[" + this.getClass().getSimpleName() + ".processSettings()]: unable to configure logger" + "; cause of exception:" + e.getCause());
				//e.printStackTrace();
			} 
		}
		logger.info("starting analysis with the following settings" 
				+ "\nblast arguments: " + configuration.getString(BLAST_ARGS) );
		
		logger.info("blast input file: " + configuration.getString(INPUT_FILE) );

		logger.info("blast output file: " + configuration.getString(BLAST_OUTPUT_FILE) );

		//logger.info("blastwrapper output file: " + configuration.getString(REPORT_FILE) );

		logger.info("saving blast output to file: " + configuration.getString( BLAST_OUTPUT_FILE ) );

		logger.info("creating no-hits file: " + configuration.getString( NO_HIT_FILE ) );
		
		//System.out.println("__" + configuration.getString( NO_HIT_FILE ) + "__");
		nohitsFile = new File(configuration.getString( NO_HIT_FILE ));
		if( nohitsFile.exists() ){
			logger.info("nohits file exists and will be deleted: " + nohitsFile.getAbsolutePath() );
			nohitsFile.delete();
		}
		nohitsFile.createNewFile();
		
		if( configuration.getString(BLAST_ARGS) != null ){
			this.blastCommand = configuration.getString(BLAST_ARGS) + " -i " + configuration.getString(INPUT_FILE) + " -o " + configuration.getString( BLAST_OUTPUT_FILE );
		}
		else{
			throw new Exception("[" + this.getClass().getSimpleName() + ".processSettings()]: unable to create blast command" );
		}
		if( ! blastCommand.contains("-m 8") ){
			throw new Exception("[" + this.getClass().getSimpleName() + ".processSettings()]: blast command out put only supported for -m 8!" );
		}
	}

    /**
     * The loadConfiguration method loads the configuration for the application from file.
     * If this configuration cannot be loaded, a ConfigurationException 
     * will be thrown.
     * It will also start the logger if it has yet been started
     * @throws Exception
     */
    private void loadConfiguration( ) throws Exception {
        try {
            File xmlConfigFile = new File( configFile );
            if (! xmlConfigFile.canRead() ) {
                throw new Exception( "unable to read configuration file: " + configFile);
            }
            configuration = new XMLConfiguration( xmlConfigFile );
        } catch (Exception e) { // configuration could not be loaded
			throw new Exception("[" + this.getClass().getSimpleName() + ".loadConfiguration()] unable to load configuration file: " + configFile + "; cause of exception:" + e.getCause() );
        }
    }

}
