/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;

import net.cellingo.utils.conversion.ValueType;

/**
 * This class encapsulates the filtering process of HSPs, the basic elements of
 * blast hits. Filter objects with set minimum and/or maximum values will filter
 * provided BlastHsp objects and return their p[assed status. 
 * @author Cellingo
 * @version 1.0
 */
public class HspPropertiesFilter {
	HashMap<HspProperty, Double> hspMinimumProperties;
	HashMap<HspProperty, Double> hspMaximumProperties;
	
	/**
	 * bean constructor
	 */
	public HspPropertiesFilter(){
		this.hspMinimumProperties = new HashMap<HspProperty, Double>();
		this.hspMaximumProperties = new HashMap<HspProperty, Double>();
	}

	public String toString(){
		StringBuilder sv = new StringBuilder();
		sv.append("[HspPropertiesFilter] START");
		sv.append("\n   Minimum values: ( ");
		//minimum values
		for( HspProperty hspProperty : hspMinimumProperties.keySet() ){
			sv.append(hspProperty.toString());
			sv.append("=");
			double minimum = hspMinimumProperties.get(hspProperty);
			NumberFormat formatter = new DecimalFormat("#0.00");
			sv.append( formatter.format(minimum) + "; ");
		}
		sv.delete(sv.length()-2, sv.length()-1);
		sv.append(") ");
		//maximum values
		sv.append("\n   Maximum values: ( ");
		for( HspProperty property : hspMaximumProperties.keySet() ){
			sv.append(property.toString());
			sv.append("=");
			double maximum = hspMaximumProperties.get(property);
			NumberFormat formatter = new DecimalFormat("#0.00");
			sv.append( formatter.format(maximum) + "; ");
		}
		sv.delete(sv.length()-2, sv.length()-1);
		sv.append(")");
		sv.append("\n[HspPropertiesFilter] END");
		return sv.toString();
	}
	
	/**
	 * Check whether a given property exists in this HSP
	 * returns true if it exists.
	 * @param hspProperty
	 * @return existence
	 */
	public boolean containsMinimumProperty( HspProperty hspProperty ){
		if( hspMinimumProperties.containsKey(hspProperty) ) return true;
		return false;
	}

	/**
	 * returns the (double) value of an Integer or Double type property
	 * @param hspProperty
	 * @return property value
	 */
	public double getMinimumPropertyValue( HspProperty hspProperty ){
		if( HspProperty.getValueType( hspProperty ) == ValueType.INTEGER  || HspProperty.getValueType( hspProperty ) == ValueType.DOUBLE ){
			if( hspMinimumProperties.containsKey(hspProperty) ){
				return ( hspMinimumProperties.get(hspProperty) );
			}
			else throw new NoSuchFieldError("Requested property (" + hspProperty + ") does not exist");
		}
		else{
			throw new IllegalArgumentException("Requested property (" + hspProperty + ") is not an Integer or Double value");
		}
	}
	
	/**
	 * set a property with an Integer or Double value
	 * @param hspProperty
	 * @param value
	 * @throws IllegalArgumentException
	 */
	public void setMinimumPropertyValue( HspProperty hspProperty, double value ) throws IllegalArgumentException{
		if( HspProperty.getValueType( hspProperty ) == ValueType.INTEGER || HspProperty.getValueType( hspProperty ) == ValueType.DOUBLE ){
			hspMinimumProperties.put(hspProperty, value);
		}
		else{
			throw new IllegalArgumentException("Property to set (" + hspProperty + ") is not an Integer value: " + value);
		}
	}

	/**
	 * Check whether a given property exists in this HSP
	 * returns true if it exists.
	 * @param hspProperty
	 * @return existence
	 */
	public boolean containsMaximumProperty( HspProperty hspProperty ){
		if( hspMaximumProperties.containsKey(hspProperty) ) return true;
		return false;
	}

	/**
	 * returns the (double) value of an Integer or Double type property
	 * @param hspProperty
	 * @return property value
	 */
	public double getMaximumPropertyValue( HspProperty hspProperty ){
		if( HspProperty.getValueType( hspProperty ) == ValueType.INTEGER  || HspProperty.getValueType( hspProperty ) == ValueType.DOUBLE ){
			if( hspMaximumProperties.containsKey(hspProperty) ){
				return ( hspMaximumProperties.get(hspProperty) );
			}
			else throw new NoSuchFieldError("Requested property (" + hspProperty + ") does not exist");
		}
		else{
			throw new IllegalArgumentException("Requested property (" + hspProperty + ") is not an Integer or Double value");
		}
	}
	
	/**
	 * set a property with an Integer or Double value
	 * @param hspProperty
	 * @param value
	 * @throws IllegalArgumentException
	 */
	public void setMaximumPropertyValue( HspProperty hspProperty, double value ) throws IllegalArgumentException{
		if( HspProperty.getValueType( hspProperty ) == ValueType.INTEGER || HspProperty.getValueType( hspProperty ) == ValueType.DOUBLE ){
			hspMaximumProperties.put(hspProperty, value);
		}
		else{
			throw new IllegalArgumentException("Property to set (" + hspProperty + ") is not an Integer or Double value: " + value);
		}
	}

	/**
	 * filters a blast hsp for minimum and maximum property values
	 * @param blastHsp
	 * @return true if within the set property ranges
	 * @throws NoSuchFieldException if the requested filter property does not exist on the BlastHsp object
	 */
	public boolean filter( BlastHsp blastHsp ) throws NoSuchFieldException{
		//minimum values
		for( HspProperty hspProperty : hspMinimumProperties.keySet() ){
			double minimum = hspMinimumProperties.get(hspProperty);
			if( blastHsp.containsProperty( hspProperty ) ){
				double hspValue = blastHsp.getPropertyValue( hspProperty );
				//System.out.println("min property value: " + minimum + " HSP value: " + hspValue );
				if( hspValue < minimum ) return false;
			}
			else{
				//System.out.println("property not present in HSP: " + hspProperty.toString() );
				throw new NoSuchFieldException( "trying to filter for a nonexistent property: " + hspProperty );
			}
		}
		//maximum values
		for( HspProperty property : hspMaximumProperties.keySet() ){
			double maximum = hspMaximumProperties.get(property);
			if( blastHsp.containsProperty( property ) ){
				double hspValue = blastHsp.getPropertyValue( property );
				//System.out.println("max property value: " + maximum + " HSP value: " + hspValue );
				if( hspValue > maximum ) return false;
			}
			else{
				//System.out.println("property not present in HSP: " + hspProperty.toString() );
				throw new NoSuchFieldException( "trying to filter for a nonexistent property: " + property );
			}

		}
		return true;
	}
}
