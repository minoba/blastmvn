/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * @author M.A. Noback (m.a.noback@pl.hanze.nl) 
 * @version 1.0
 */
public class BlastResults {

	
	/**
	 * map of queryID Strings and Query objects
	 */
	private HashMap<String, BlastQuery> queries;
	//private int queryCount;

	/**
	 * no-arg constructor
	 */
	public BlastResults(){
		queries = new HashMap<String, BlastQuery>();
	}
	
	/**
	 * add a blast hit for a query. A new BlastQuery is created if it does not yet exist
	 * @param queryID
	 * @param hit
	 */
	public void addHit( String queryID, BlastHit hit){
		if( queries.containsKey(queryID) ){
			queries.get(queryID).addBlastHit(hit);
		}
		else{
			BlastQuery query = new BlastQuery( queryID );
			query.addBlastHit(hit);
			queries.put(queryID, query);
		}
		queries.get(queryID).setHitStatus(BlastQuery.STATUS_WITH_HIT);
	}
	/**
	 * add a query to the collection
	 * @param query
	 */
	public void addQuery( BlastQuery query ) throws NoSuchFieldError{
		if( query.getQueryId() == null ) throw new NoSuchFieldError("trying to add a query without a queryID");
		//System.out.println("adding query " + query.getQueryId() );
		queries.put( query.getQueryId(), query );
	}
	/**
	 * returns an existing query with this queryDefinition or null
	 * if it does not yet exist.
	 * @param queryDefinition
	 * @return blast query
	 */
	public BlastQuery getQuery(String queryDefinition) {
		if( queries.containsKey( queryDefinition )){
			return queries.get( queryDefinition );
		}
		return null;
	}

	
	/**
	 * add a query ID to create a list of queries for this job. It gets a queue number 
	 * so that queries can be retrieved in the order in which they were entered. 
	 * The query hit status is set to default: BlastConfiguration.QUERY_STATUS_NO_HIT
	 * @param queryID
	 */
	public BlastQuery createQuery(String queryID) {
		BlastQuery query = new BlastQuery( queryID );
		queries.put(queryID, query);
		return query;
	}
	/**
	 * Set the hit status of a query. If no Query object exists yet with this ID, one will be created.
	 * Use static final fields
	 * BlastConfiguration.QUERY_STATUS_NO_HIT or BlastConfiguration.QUERY_STATUS_HIT
	 * @param queryID
	 * @param status
	 */
	public void setQueryHitStatus( String queryID, int status ){
		if(queries.containsKey(queryID)){
			queries.get(queryID).setHitStatus(status);
		}
		BlastQuery query = new BlastQuery( queryID );
		query.setHitStatus(status);
		queries.put( queryID, query );
	}
	/**
	 * Returns the hit status of a query. Compare to static final fields 
	 * BlastConfiguration.QUERY_STATUS_NO_HIT or BlastConfiguration.QUERY_STATUS_HIT
	 * @param queryID
	 * @return hit status
	 */
	public int getQueryHitStatus( String queryID ){
		if( ! queries.containsKey(queryID) ){
			throw new NullPointerException("unknown query");
		}
		return queries.get(queryID).getHitStatus();
	}
	/**
	 * get an iterator of all queries in this results object. This includes both 
	 * those with and without hits.
	 * @return iterator of queries
	 */
	public Iterator<BlastQuery> getQueries(){
		List<BlastQuery> qrs = new ArrayList<BlastQuery>();
		qrs.addAll(queries.values());
		Collections.sort(qrs);
		return qrs.iterator();
	}
	
	/**
	 * Returns an Iterator of query IDs that yielded no hits in the current blast job.
	 * List is sorted lexicographically.
	 * @return iterator of query IDs
	 */
	public Iterator<BlastQuery> getQueriesWithoutHits(){
		List<BlastQuery> noHits = new ArrayList<BlastQuery>();
		for( BlastQuery query : queries.values() ){
			//System.out.println("query: " + query.getQueryId() );
			if( query.getHitStatus() == BlastQuery.STATUS_NO_HIT ) noHits.add( query ) ;
		}
		Collections.sort(noHits);
		return noHits.iterator();
	}
	
	/**
	 * returns an iterator query objects with blast hits
	 * @return an iterator query objects with blast hits
	 */
	public Iterator<BlastQuery> getQueriesWithHits(){
		List<BlastQuery> hits = new ArrayList<BlastQuery>();
		for( BlastQuery query : queries.values() ){
			//System.out.println("query: " + query.getQueryId() );
			if( query.getHitStatus() == BlastQuery.STATUS_WITH_HIT ){
				//System.out.println("hit: " + query.getHitStatus() );
				hits.add( query ) ;
			}
		}
		Collections.sort(hits);
		return hits.iterator();
	}

	/**
	 * removes the query from the collection. For use in streaming processing
	 * @param query
	 */
	public void removeQuery(BlastQuery query) {
		queries.remove( query.getQueryId() );
	}
}
