/**
 * 
 */
package net.cellingo.sequence_tools.blast;

/**
 * This enum defines all possible elements in a NCBI blast XML output file 
 * @author Cellingo
 * @version 1.0
 */
public enum BlastElement {
	BLAST_ITERATION("Iteration"),
	BLAST_ITERATION_NUMBER("Iteration_iter-num"),
	BLAST_ITERATION_QUERY_ID("Iteration_query-ID"),
	BLAST_ITERATION_QUERY_DEFINITION("Iteration_query-def"),
	BLAST_ITERATION_QUERY_LENGTH("Iteration_query-len"),
	
	HIT("Hit"),
	HIT_NUMBER("Hit_num"),
	HIT_ID("Hit_id"),
	HIT_DEFINITION("Hit_def"),
	HIT_ACCESSION("Hit_accession"),
	HIT_LENGTH("Hit_len"),

	HSP("Hsp"),
	HSP_NUMBER("Hsp_num"),
	HSP_BIT_SCORE("Hsp_bit-score"),
	HSP_SCORE("Hsp_score"),
	HSP_EVALUE("Hsp_evalue"),
	HSP_QUERY_FROM("Hsp_query-from"),
	HSP_QUERY_TO("Hsp_query-to"),
	HSP_HIT_FROM("Hsp_hit-from"),
	HSP_HIT_TO("Hsp_hit-to"),
	HSP_QUERY_FRAME("Hsp_query-frame"),
	HSP_HIT_FRAME("Hsp_hit-frame"),
	HSP_IDENTITIES("Hsp_identity"),
	HSP_POSITIVES("Hsp_positive"),
	HSP_ALIGN_LENGTH("Hsp_align-len"),
	HSP_QUERY_SEQUENCE("Hsp_qseq"),
	HSP_HIT_SEQUENCE("Hsp_hseq"),
	HSP_MIDLINE("Hsp_midline");

	private String type;
	
	private BlastElement( String type ){
		this.type = type;
	}
	
	public String toString(){
		return type;
	}
}
