/**
 * 
 */
package net.cellingo.sequence_tools.blast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

/**
 * This class can parse NCBI blast xml output. 
 * It implements the BlastParser interface and exposes only those methods. 
 * @author Cellingo
 * @version 1.0
 */

public class NcbiBlastParser implements BlastParser {

	
	private File file;
	private XMLEventReader reader;
	private ParserListener listener;
	private BlastResults blastResults;
	private int maxHsps = Integer.MAX_VALUE;

	/**
	 * no-arg constructor
	 */
	public NcbiBlastParser( ){ }

	/**
	 * construct with the input file that should be parsed
	 * @param file
	 */
	public NcbiBlastParser( File file ){
		this.file = file;
		
	}
	
	/**
	 * Call this to start the parsing process
	 */
	public void parse() throws Exception{//throws XMLReaderException
		if( file == null ){
			throw new Exception("no input file is defined");
		}
		if(blastResults == null) blastResults = new BlastResults();
		try {
			XMLInputFactory factory = XMLInputFactory.newInstance();
			factory.setProperty(javax.xml.stream.XMLInputFactory.IS_COALESCING, Boolean.TRUE);
			//reader = factory.createXMLEventReader( , new FileInputStream(file));
			reader = factory.createXMLEventReader( file.getName(), new BufferedInputStream( new FileInputStream( file) ) );

			//System.out.println("parsing");
			XMLEvent event;
			while(reader.hasNext()) {
				event = reader.nextEvent();
				if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.BLAST_ITERATION.toString() ) ){//BlastElement.BLAST_ITERATION.toString()  "Iteration"
					//System.out.println("parsing iteration");
					parseIteration( );
				}
			} 
		} catch (FileNotFoundException e1) {
			//e1.printStackTrace();
			throw new IOException("file could not be read\n" + e1.getCause());
			//new XMLReaderException("file could not be read\n" + e1.getCause());
		} catch (XMLStreamException e1) {
			//e1.printStackTrace();
			throw new IOException("XML stream error occurred\n" + e1.getCause());
		} 
	}
	
	/**
	 * this method organises the parsing of single blast iterations
	 * @throws XMLStreamException
	 */
	private void parseIteration( ) throws XMLStreamException{
		String queryDefinition = null;
		BlastQuery query = null;
		int queryLength = 0;

		while( reader.hasNext() ){
			
			XMLEvent event = reader.nextEvent();
			/*check for end-of iteration*/
			if( (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals( BlastElement.BLAST_ITERATION.toString() ) ) ){

				if(query != null){
					//System.out.println("query " + query.getQueryId() + " parsed; hsp number= " + query.getHspNumber());
					//query.setHitStatus(BlastQuery.STATUS_WITH_HIT);
					blastResults.addQuery(query);
					if( listener != null ){
						listener.iterationReady( query );
					}
				}
/*				else{
					System.out.println("NULL query at " + event.getLocation().getLineNumber() );
				}
*/
				return;
			}
			/*read iteration data*/
/*			if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.BLAST_ITERATION_NUMBER.toString() ) ){
				event = reader.nextEvent();
				if(event.isCharacters()){
					create new query entry in HashMap
					int iterationNumber = Integer.parseInt( event.asCharacters().toString() );
					query.setIterationNumber(iterationNumber);
					//System.out.println("iteration number: " + event.asCharacters().toString() );
				}
			}
*/			if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.BLAST_ITERATION_QUERY_DEFINITION.toString() ) ){
				event = reader.nextEvent();
				if(event.isCharacters()){
					queryDefinition = event.asCharacters().toString();
					
					query = blastResults.getQuery( queryDefinition );
					if(query == null) query = new BlastQuery();
					query.setQueryId(queryDefinition);
					//System.out.println("query definition: " + query.getQueryId() );
				}
			}
/*			if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.BLAST_ITERATION_QUERY_ID.toString() ) ){
				event = reader.nextEvent();
				if(event.isCharacters()){
					//System.out.println("query id: " + event.asCharacters().toString() );
				}
			} */
			else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.BLAST_ITERATION_QUERY_LENGTH.toString() ) ){
				event = reader.nextEvent();
				if(event.isCharacters()){
					queryLength = Integer.parseInt(  event.asCharacters().toString() );
					query.setQueryLength( queryLength );
					//System.out.println("query length: " + event.asCharacters().toString() );
				}
			}
			/*proceed into hit-data*/
			else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HIT.toString() ) ){
				query.setHitStatus(BlastQuery.STATUS_WITH_HIT);
				parseHit( query );
			}
		}
	}
	
	private void parseHit( BlastQuery query ) throws XMLStreamException{
		//System.out.println("\n\n##########   parsing hit for " + query.getQueryId());
		/*if the maximum number of hsps has been reached, find end of this hit and return*/
		if(query.getHspNumber() >= maxHsps){
			XMLEvent event;
			while( reader.hasNext() ){	
				event = reader.nextEvent();
				/*check for end of hit*/
				if( (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals( BlastElement.HIT.toString() ) ) ){
					return;
				}
			}
		}
		else{
			BlastHit blastHit = new BlastHit();
			query.addBlastHit( blastHit );
			
			while( reader.hasNext() ){	
				XMLEvent event = reader.nextEvent();
				/*check for end of hit*/
				if( (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals( BlastElement.HIT.toString() ) ) ){
					return;
				}
				/*read hit data*/
	/*			if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HIT_NUMBER.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						System.out.println("   hit number: " + event.asCharacters().toString() );
					}
				}
	*/			
				if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HIT_ID.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						blastHit.setHitID( event.asCharacters().toString() );
						//System.out.println("   hit ID: " + event.asCharacters().toString() );
					}
				}
				/*TODO entities like "&apos;" are also events!!! this causes truncation of the hit def*/
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HIT_DEFINITION.toString() ) ){
					String hitDef = "";
					while( reader.hasNext() && !(event.isEndElement() && event.asEndElement().getName().getLocalPart().equals( BlastElement.HIT_DEFINITION.toString() ) ) ){
						event = reader.nextEvent();
						if(event.isCharacters() || event.isEntityReference() ){
							hitDef += event.asCharacters();
							//blastHit.setHitDefinition( event.asCharacters().toString() );
							//System.out.println("   hit  definition: " + event.asCharacters().toString() );
						}
					}
					//System.out.println("   hit  definition: " + hitDef );
					blastHit.setHitDefinition( hitDef);
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HIT_ACCESSION.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						blastHit.setHitAccession( event.asCharacters().toString() );
						//System.out.println("   hit accession: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HIT_LENGTH.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						blastHit.setHitLength( Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("   hit length: " + event.asCharacters().toString() );
					}
				}
				/*proceed into hsp-data*/
				if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP.toString() ) ){
					parseHsp( blastHit, query );
				}
			}
		}
	}
	
	private void parseHsp(BlastHit blastHit, BlastQuery query ) throws XMLStreamException{
		/*if the maximum number of hsps has been erached, return immediately*/
		if(query.getHspNumber()>=maxHsps){
			XMLEvent event;
			while( reader.hasNext() ){	
				event = reader.nextEvent();
				/*check for end of hit*/
				if( (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals( BlastElement.HSP.toString() ) ) ){
					return;
				}
			}
		}
		else{
			BlastHsp hsp = new BlastHsp();
			//this property is used for determining derivative properties
			hsp.setIntegerPropertyValue( HspProperty.QUERY_LENGTH, query.getQueryLength() );
/*			try {
				hsp.setStringPropertyValue( HspProperty.HIT_ID, blastHit.getHitID() );
			} catch (Exception e) {
				hsp.setStringPropertyValue( HspProperty.HIT_ID, "unknown ID" );
			}
*/			blastHit.addHsp(hsp);
			
			while( reader.hasNext() ){
				
				XMLEvent event = reader.nextEvent();
				/*check for end of hit*/
				if( (event.isEndElement() && event.asEndElement().getName().getLocalPart().equals( BlastElement.HSP.toString() ) ) ){
					return;
				}
				/*read hit data*/
				if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_NUMBER.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_NUMBER , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp number: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_BIT_SCORE.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setDoublePropertyValue( HspProperty.HSP_BIT_SCORE , Double.parseDouble( event.asCharacters().toString() ) );
						//System.out.println("      hsp bit score: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_SCORE.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_SCORE , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp score: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_EVALUE.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setDoublePropertyValue( HspProperty.HSP_EVALUE , Double.parseDouble( event.asCharacters().toString() ) );
/*						try {
							System.out.println("      hsp e-value: " + hsp.getDoublePropertyValue(HspProperty.HSP_EVALUE) );
						} catch (Exception e) {
							e.printStackTrace();
						}
*/					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_QUERY_FROM.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_QUERY_FROM , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp query from: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_QUERY_TO.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_QUERY_TO , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp query to: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_HIT_FROM.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_HIT_FROM , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp hit from: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_HIT_TO.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_HIT_TO , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp hit to: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_QUERY_FRAME.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_QUERY_FRAME , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp query frame: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_HIT_FRAME.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_HIT_FRAME , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp hit frame: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_IDENTITIES.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_IDENTITIES , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp identities: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_POSITIVES.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_POSITIVES , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp positives: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_ALIGN_LENGTH.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setIntegerPropertyValue( HspProperty.HSP_ALIGN_LENGTH , Integer.parseInt( event.asCharacters().toString() ) );
						//System.out.println("      hsp align length: " + event.asCharacters().toString() );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_QUERY_SEQUENCE.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setStringPropertyValue( HspProperty.HSP_QUERY_SEQUENCE, event.asCharacters().toString() );
						//System.out.println("***********hsp query sequence (" + event.asCharacters().toString().length() + ")" );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_HIT_SEQUENCE.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setStringPropertyValue( HspProperty.HSP_HIT_SEQUENCE, event.asCharacters().toString() );
						//System.out.println("***********hsp hit sequence (" + event.asCharacters().toString().length() + ")" );
					}
				}
				else if( event.isStartElement() && event.asStartElement().getName().getLocalPart().equals( BlastElement.HSP_MIDLINE.toString() ) ){
					event = reader.nextEvent();
					if(event.isCharacters()){
						hsp.setStringPropertyValue( HspProperty.HSP_MIDLINE, event.asCharacters().toString() );
						//System.out.println("***********hsp midline from xml file (" + event.asCharacters().toString().length() + ")" );
						
					}
				}
			}
		}
	}

	//@Override
	public BlastResults getBlastResults(){
		return blastResults;
	}
	
	//@Override
	public void setBlastResults( BlastResults blastResults ){
		this.blastResults = blastResults;
	}
	
	//@Override
	public void setFile(File file) {
		this.file = file;
	}

	//@Override
	public void setMaxHsps(int maxHsps) {
		this.maxHsps = maxHsps;
	}

	//@Override
	public void parseStreaming( ParserListener listener ) {
		this.listener = listener;
	}
	
}
