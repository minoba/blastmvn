/**
 * 
 */
package net.cellingo.sequence_tools.blast_testers;

import net.cellingo.sequence_tools.blast.HspProperty;

/**
 * @author michiel
 *
 */
public class MiscTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String prop = "HSP_NUMBER";
		
		System.out.println( "return value of " + prop + " is: " + HspProperty.valueOf(prop) );
	}

}
