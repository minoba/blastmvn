package net.cellingo.sequence_tools.blast_testers;

import java.io.File;
import java.util.Iterator;

import net.cellingo.sequence_tools.blast.BlastHit;
import net.cellingo.sequence_tools.blast.BlastHsp;
import net.cellingo.sequence_tools.blast.BlastParser;
import net.cellingo.sequence_tools.blast.BlastQuery;
import net.cellingo.sequence_tools.blast.BlastResults;
import net.cellingo.sequence_tools.blast.HspProperty;
import net.cellingo.sequence_tools.blast.NcbiBlastParser;


public class BlastXmlReaderTester {

	public static void main(String[] args){
		BlastXmlReaderTester bxrt = new BlastXmlReaderTester();
		bxrt.start();
		
	}
	public void start(){
		BlastParser bp = new NcbiBlastParser(new File("/share/home/michiel/Desktop/output_alignment_0_reference_AP_000571.xml") );
		System.out.println("starting...");
		try {
			bp.parse();
			BlastResults br = bp.getBlastResults();
			Iterator<BlastQuery> queries = br.getQueries();
			while(queries.hasNext()){
				 BlastQuery bq = queries.next();
				 System.out.println("QUERY: " + bq.getQueryId());
				 Iterator<BlastHit> blasthits = bq.getBlastHits();
				 while(blasthits.hasNext()){
					 BlastHit bh = blasthits.next();
					 System.out.println("HIT: " + bh.getHitID());
					 Iterator<BlastHsp> hsps = bh.getHsps();
					 while(hsps.hasNext()){
						 BlastHsp hsp = hsps.next();
						 System.out.println("HSP: ");
						 String midLine = hsp.getStringPropertyValue(HspProperty.HSP_MIDLINE);
						 String querySequence = hsp.getStringPropertyValue(HspProperty.HSP_QUERY_SEQUENCE);
						 String referenceSequence = hsp.getStringPropertyValue(HspProperty.HSP_HIT_SEQUENCE);
						 //System.out.println(midLine+"\n"+querySequence+"\n"+referenceSequence+"\n");
						 System.out.println("length of query: "+querySequence.length()+" length of reference: "+referenceSequence.length()+" length of midline: "+midLine.length());
					 }
					 
				 }
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
