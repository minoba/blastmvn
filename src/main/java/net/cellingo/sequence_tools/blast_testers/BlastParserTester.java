/**
 * 
 */
package net.cellingo.sequence_tools.blast_testers;

import java.io.File;
import java.util.Iterator;

import net.cellingo.sequence_tools.blast.BlastHit;
import net.cellingo.sequence_tools.blast.BlastParser;
import net.cellingo.sequence_tools.blast.BlastQuery;
import net.cellingo.sequence_tools.blast.BlastResults;
import net.cellingo.sequence_tools.blast.HspPropertiesFilter;
import net.cellingo.sequence_tools.blast.NcbiBlastParser;
import net.cellingo.sequence_tools.blast.ParserListener;

/**
 * @author Cellingo
 *
 */
public class BlastParserTester implements ParserListener {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		BlastParserTester tester = new BlastParserTester();
		try {
			tester.test( args[0] );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void test( String fileName ) throws Exception{
		File file = new File( fileName );
		
		/*set up filter properties*/
		HspPropertiesFilter filter = new HspPropertiesFilter();
		//filter.setMinimumPropertyValue(HspProperty.HSP_ALIGN_LENGTH, 100);
		//filter.setMinimumPropertyValue( HspProperty.HSP_IDENTITY_PERCENTAGE, 95);
		//filter.setMinimumPropertyValue(HspProperty.HSP_ALIGN_PERCENTAGE, 100.0);
		//filter.setMaximumPropertyValue( HspProperty.HSP_MISMATCH_NUMBER, 100);
		//filter.setMaximumPropertyValue( HspProperty.HSP_EVALUE, 1.0E-50);
		//filter.setMaximumPropertyValue( HspProperty.HSP_MISMATCH_PERCENTAGE, 5.0);

		
		BlastParser parser = new NcbiBlastParser( );
		parser.setFile( file );
		
//		parser.parseStreaming(true, this);
		
		parser.parse();
		BlastResults results = parser.getBlastResults();
		Iterator<BlastQuery> queries = results.getQueriesWithHits();
		while( queries.hasNext() ){
			BlastQuery query = queries.next();
			System.out.println("query:" + query.getQueryId() );
			Iterator<BlastHit> hits = query.getBlastHits();
			while( hits.hasNext() ){
				BlastHit hit = hits.next();
				try {
					System.out.println("   hit:" + hit.getHitDefinition() );
					//filter results
					System.out.println("   hit first HSP OK: " + filter.filter( hit.getFirstHsp() ) );
					//filter.filter( hit.getFirstHsp() );
				} catch (NoSuchFieldException e) {
					e.printStackTrace();
				}
			}
		}

		
	}
	
	//@Override
	public void iterationReady( BlastQuery query ) {
		System.out.println( "iteration ready: " + query.getQueryId() );
		
	}

}
