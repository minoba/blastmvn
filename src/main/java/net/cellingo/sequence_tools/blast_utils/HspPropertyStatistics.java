package net.cellingo.sequence_tools.blast_utils;

import java.text.NumberFormat;

import net.cellingo.sequence_tools.blast.HspProperty;
import net.cellingo.utils.conversion.ValueType;

public class HspPropertyStatistics {
	private HspProperty property;
	private ValueType valueType;
	private int valueCount = 0;
//	private ArrayList<Double> cumulativeIntValues = new ArrayList<Double>();
//	private ArrayList<Double> intValueStops = new ArrayList<Double>();
	private Bucket[] buckets;
	private double minimum;
	private double maximum;
	private int intervals;
	private NumberFormat nf;
	private boolean useLogs;
	
	/**
	 * @return the minimum
	 */
	public double getMinimum() {
		return minimum;
	}


	/**
	 * @return the maximum
	 */
	public double getMaximum() {
		return maximum;
	}


	/**
	 * 
	 * @param property
	 * @param min the minimum value
	 * @param max the maximum value
	 * @param intervals the number of intervals to create
	 */
	public HspPropertyStatistics(HspProperty property, double min, double max, int intervals){
		this.property = property;
		this.valueType = HspProperty.getValueType(property);
		if( ! ( valueType == ValueType.DOUBLE || valueType == ValueType.INTEGER ) ){
			System.err.println("cannot calculate statistics for property " + property);
		}
		if( property == HspProperty.HSP_EVALUE ){
			this.useLogs = true;
			this.minimum = Math.log10(min);
			this.maximum = Math.log10(max);
			
		}else{
			this.minimum = min;
			this.maximum = max;
		}
		this.intervals = intervals;
		
		this.nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		
		
		createBuckets();
	}
	
	
	private void createBuckets(){
		buckets = new Bucket[intervals];
		double step = (this.maximum - this.minimum) / this.intervals;
		for( int i=0; i<intervals; i++){
			double min = this.minimum + (i * step);
			Bucket b = new Bucket(min, min+step);
			buckets[i] = b;
		}
	}
	
	public Bucket getBucket(int index){
		return buckets[index];
	}
	
	public void addValue( double value ){
		valueCount++;
		if( this.useLogs ){
			value = Math.log10(value);
		}

		for( int i=0; i<buckets.length; i++){//Bucket b : buckets){
			Bucket b = buckets[i];
			if( i == buckets.length-1 ){
				b.addValue(value);
				break;
			}
			if( value >= b.bminimum && value < b.bmaximum){
				b.addValue(value);
				break;
			}
		}
	}
	
	/**
	 * @return the valueCount
	 */
	public int getValueCount() {
		return valueCount;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		
		sb.append("HspPropertyStatistics for " + this.property);
		for( Bucket b : buckets ){
			sb.append("\n");
			sb.append(b.toString());
		}
		sb.append("\n");
		
		return sb.toString();
	}
	
	public HspProperty getHspProperty(){
		return this.property;
	}
	
	public class Bucket{
		final double bminimum;
		final double bmaximum;
		double sum;
		int count;
		

		public Bucket( double minimum, double maximum){
			this.bminimum = minimum;
			this.bmaximum = maximum;
		}
		
		public void addValue(double value){
			count++;
			sum += value;
		}
		
		public String toString(){
			double avg = (count==0 ? 0 : sum/count);
			return "Bucket[min=" + nf.format(bminimum) + " max=" + nf.format(bmaximum) + " count=" + count 
				+ " sum=" + nf.format(sum) + " avg=" + nf.format(avg) + "]";
		}
	}
}
