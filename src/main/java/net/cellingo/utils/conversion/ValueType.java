/**
 * 
 */
package net.cellingo.utils.conversion;

/**
 * @author Cellingo
 *
 */
public enum ValueType {
	INTEGER("Integer"),
	DOUBLE("Double"),
	STRING("String"),
	COMPLEX("Complex"),
	UNKNOWN("Unknown");
	
	private String type;
	
	private ValueType(String type){
		this.type = type;
	}
	
	public String toString(){
		return type;
	}
}
